package edu.upc.mpi.antlr;

// Generated from LogicSchemaGrammarAmpliat.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LogicSchemaGrammarAmpliatLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		NOT=1, OR=2, FALSE=3, OPERATOR=4, ID=5, CONSTRAINTID=6, NEWLINE=7, WS=8, 
		COMMENT=9, COMMA=10, OPENPAR=11, CLOSEPAR=12, ARROWNORMALCLAUSE=13, ARROWRGD=14, 
		EXTRAINFO=15;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"NOT", "OR", "FALSE", "OPERATOR", "ID", "CONSTRAINTID", "NEWLINE", "WS", 
			"COMMENT", "COMMA", "OPENPAR", "CLOSEPAR", "ARROWNORMALCLAUSE", "ARROWRGD", 
			"EXTRAINFO"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'not'", "' v '", "'false'", null, null, null, null, null, null, 
			"','", "'('", "')'", "':-'", "'->'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "NOT", "OR", "FALSE", "OPERATOR", "ID", "CONSTRAINTID", "NEWLINE", 
			"WS", "COMMENT", "COMMA", "OPENPAR", "CLOSEPAR", "ARROWNORMALCLAUSE", 
			"ARROWRGD", "EXTRAINFO"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public LogicSchemaGrammarAmpliatLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "LogicSchemaGrammarAmpliat.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\21u\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\3\2\3\2\3\2\3\2\3\3"+
		"\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5"+
		"\58\n\5\3\6\6\6;\n\6\r\6\16\6<\3\7\3\7\6\7A\n\7\r\7\16\7B\3\b\5\bF\n\b"+
		"\3\b\3\b\3\t\6\tK\n\t\r\t\16\tL\3\t\3\t\3\n\3\n\7\nS\n\n\f\n\16\nV\13"+
		"\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\16\3\17\3\17\3\17\3\20\3\20\7"+
		"\20f\n\20\f\20\16\20i\13\20\3\20\3\20\3\20\7\20n\n\20\f\20\16\20q\13\20"+
		"\3\20\5\20t\n\20\2\2\21\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f"+
		"\27\r\31\16\33\17\35\20\37\21\3\2\7\4\2>>@@\t\2%%))//\62;C\\aac|\3\2\62"+
		";\4\2\13\13\"\"\4\2\f\f\17\17\2\u0080\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2"+
		"\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2"+
		"\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3"+
		"\2\2\2\2\37\3\2\2\2\3!\3\2\2\2\5%\3\2\2\2\7)\3\2\2\2\t\67\3\2\2\2\13:"+
		"\3\2\2\2\r>\3\2\2\2\17E\3\2\2\2\21J\3\2\2\2\23P\3\2\2\2\25W\3\2\2\2\27"+
		"Y\3\2\2\2\31[\3\2\2\2\33]\3\2\2\2\35`\3\2\2\2\37s\3\2\2\2!\"\7p\2\2\""+
		"#\7q\2\2#$\7v\2\2$\4\3\2\2\2%&\7\"\2\2&\'\7x\2\2\'(\7\"\2\2(\6\3\2\2\2"+
		")*\7h\2\2*+\7c\2\2+,\7n\2\2,-\7u\2\2-.\7g\2\2.\b\3\2\2\2/8\7?\2\2\60\61"+
		"\7>\2\2\618\7@\2\2\628\t\2\2\2\63\64\7>\2\2\648\7?\2\2\65\66\7@\2\2\66"+
		"8\7?\2\2\67/\3\2\2\2\67\60\3\2\2\2\67\62\3\2\2\2\67\63\3\2\2\2\67\65\3"+
		"\2\2\28\n\3\2\2\29;\t\3\2\2:9\3\2\2\2;<\3\2\2\2<:\3\2\2\2<=\3\2\2\2=\f"+
		"\3\2\2\2>@\7B\2\2?A\t\4\2\2@?\3\2\2\2AB\3\2\2\2B@\3\2\2\2BC\3\2\2\2C\16"+
		"\3\2\2\2DF\7\17\2\2ED\3\2\2\2EF\3\2\2\2FG\3\2\2\2GH\7\f\2\2H\20\3\2\2"+
		"\2IK\t\5\2\2JI\3\2\2\2KL\3\2\2\2LJ\3\2\2\2LM\3\2\2\2MN\3\2\2\2NO\b\t\2"+
		"\2O\22\3\2\2\2PT\7\'\2\2QS\n\6\2\2RQ\3\2\2\2SV\3\2\2\2TR\3\2\2\2TU\3\2"+
		"\2\2U\24\3\2\2\2VT\3\2\2\2WX\7.\2\2X\26\3\2\2\2YZ\7*\2\2Z\30\3\2\2\2["+
		"\\\7+\2\2\\\32\3\2\2\2]^\7<\2\2^_\7/\2\2_\34\3\2\2\2`a\7/\2\2ab\7@\2\2"+
		"b\36\3\2\2\2cg\7]\2\2df\n\6\2\2ed\3\2\2\2fi\3\2\2\2ge\3\2\2\2gh\3\2\2"+
		"\2hj\3\2\2\2ig\3\2\2\2jt\7_\2\2ko\7}\2\2ln\n\6\2\2ml\3\2\2\2nq\3\2\2\2"+
		"om\3\2\2\2op\3\2\2\2pr\3\2\2\2qo\3\2\2\2rt\7\177\2\2sc\3\2\2\2sk\3\2\2"+
		"\2t \3\2\2\2\f\2\67<BELTgos\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}