/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.antlr;

import edu.upc.mpi.antlr.LogicSchemaGrammarAmpliatParser.AtomContext;
import edu.upc.mpi.augmented_logicschema.EventPredicate;
import edu.upc.mpi.augmented_logicschema.EventPredicate.EventType;
import edu.upc.mpi.logicschema.Literal;
import edu.upc.mpi.logicschemaRGD.LogicSchemaRGD;
import edu.upc.mpi.logicschema.Atom;
import edu.upc.mpi.logicschema.BuiltInLiteral;
import edu.upc.mpi.logicschema.DerivationRule;
import edu.upc.mpi.logicschema.LogicConstraint;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschema.Predicate;
import edu.upc.mpi.logicschema.PredicateImpl;
import edu.upc.mpi.logicschema.Term;
import edu.upc.mpi.logicschemaRGD.EGD;
import edu.upc.mpi.logicschemaRGD.LabelledNull;
import edu.upc.mpi.logicschemaRGD.RGD;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Guillermo Ezcurra
 */
public class LogicSchemaGrammarAmpliatVisitorImpl extends LogicSchemaGrammarAmpliatBaseVisitor {
    private LogicSchemaRGD logicSchemaRGD;
    /**
     * Constructs a LogicSchemaGrammarAmpliatVisitorImpl
     * @param schema esquema buit en què afegirem les RGD, EGD i predicats.
     */
    public LogicSchemaGrammarAmpliatVisitorImpl(LogicSchemaRGD schema){
        logicSchemaRGD = schema;
    }
    
    @Override public List<Literal> visitBody(LogicSchemaGrammarAmpliatParser.BodyContext ctx) {
        List<Literal> literals = new LinkedList();
        for(LogicSchemaGrammarAmpliatParser.LiteralContext litContext: ctx.literalsList().literal()){
            literals.add((Literal) this.visitLiteral(litContext));
        }
        return literals;
    }
    
    @Override public Atom visitAtom(LogicSchemaGrammarAmpliatParser.AtomContext ctx) {
        Predicate predicate = (Predicate) this.visitPredicate(ctx.predicate());
        List<Term> terms = this.visitTermsList(ctx.termsList());
      
        return new Atom(predicate, terms);
    }
    
    @Override public List<Term> visitTermsList(LogicSchemaGrammarAmpliatParser.TermsListContext ctx) {
        List<Term> terms = new LinkedList();
        for(LogicSchemaGrammarAmpliatParser.TermContext termContext: ctx.term()){
            terms.add(this.visitTerm(termContext));
        }
        return terms;
    }
    
    
    @Override public Term visitTerm(LogicSchemaGrammarAmpliatParser.TermContext ctx) {
        if(ctx.ID().getText().equals("null"))return new LabelledNull();
        else if(ctx.ID().getText().startsWith("#"))return new LabelledNull();
        else return new Term(ctx.ID().getText());
    }
    
    @Override public Predicate visitPredicate(LogicSchemaGrammarAmpliatParser.PredicateContext ctx) {
        String name = ctx.ID().getText();
        //cerquem el predicat al logic schema.
        Predicate result = logicSchemaRGD.getPredicate(name);
        //Si no està registrat al logic schema mirem la seva aritat, mirem si és una inserció o esborrament,
        //altrament, l'afegim al logicSchema. Si ja hi era, no fem res.
        if(result == null){
            int arity = ((AtomContext)ctx.getParent()).termsList().term().size();

            if(name.startsWith("ins_") || name.startsWith("del_")){
                EventType eventType = name.startsWith("ins_")?EventType.INSERT:EventType.DELETE;
                String realName = name.substring("ins_".length());
                Predicate realPredicate = logicSchemaRGD.getPredicate(realName);
                if(realPredicate == null){
                    realPredicate = new PredicateImpl(realName, arity);
                    logicSchemaRGD.addPredicate(realPredicate);
                }
                result = new EventPredicate(realPredicate, eventType);
                logicSchemaRGD.addPredicate(result);
            }
            else {
                result = new PredicateImpl(name, arity);
            }

            logicSchemaRGD.addPredicate(result);
        }
        return result;
    }
    
    @Override public LogicConstraint visitConstraint(LogicSchemaGrammarAmpliatParser.ConstraintContext ctx) {
        // Extraiem el cos de la restricció (la llista de literals)
        List<Literal> body = visitBody(ctx.body());

        LogicConstraint constraint;
        //Si l'ID de la restricció no és null l'identifiquem i creem la restricció
        if(ctx.CONSTRAINTID() != null){
            String number = ctx.CONSTRAINTID().getText().replace("@", "");
            int id = Integer.parseInt(number);
            constraint = new LogicConstraint(id, body);
        } 
        // si era null simplement creem la restricció i la funció li assigna un ID.
        else {
            constraint = new LogicConstraint(body);
        }
        // afegim la restricció a l'esquema lògic.
        logicSchemaRGD.addConstraint(constraint);
        return constraint;
    }
    
    @Override public DerivationRule visitDerivationRule(LogicSchemaGrammarAmpliatParser.DerivationRuleContext ctx) {
        Atom head = this.visitAtom(ctx.atom());
        List<Literal> body = this.visitBody(ctx.body());
        return new DerivationRule(head, body);
    }
    @Override public LogicSchemaRGD visitLine(LogicSchemaGrammarAmpliatParser.LineContext ctx) {
        visitChildren(ctx);
      
        return this.logicSchemaRGD;
    }
    @Override public LogicSchemaRGD visitProg(LogicSchemaGrammarAmpliatParser.ProgContext ctx) {
        visitChildren(ctx);
      
        return this.logicSchemaRGD;
    }
    @Override public OrdinaryLiteral visitOrdinaryLiteral(LogicSchemaGrammarAmpliatParser.OrdinaryLiteralContext ctx) {
        boolean isPositive = ctx.NOT() == null;
        Atom atom = this.visitAtom(ctx.atom());
        OrdinaryLiteral ol = new OrdinaryLiteral(atom, isPositive);
        //Afegim l'ordinary literal als fets d'entrada només si té constants, és a dir, si no és part d'una regla RGD
        logicSchemaRGD.addFetEntrada(ol);
        return ol;
    }
    @Override public BuiltInLiteral visitBuiltInLiteral(LogicSchemaGrammarAmpliatParser.BuiltInLiteralContext ctx) {
        Term leftTerm = this.visitTerm(ctx.term(0));
        Term rightTerm = this.visitTerm(ctx.term(1));
        String operator = ctx.OPERATOR().getText();
        return new BuiltInLiteral(leftTerm, rightTerm, operator);
    }
    
    @Override public RGD visitRgd(LogicSchemaGrammarAmpliatParser.RgdContext ctx) {
        List<Literal> part_esquerra = this.visitLiteralsList(ctx.literalsList());
        List<List<Literal>> part_dreta = new LinkedList<>();
        if(ctx.FALSE() == null){
        part_dreta = new LinkedList(this.visitOrdinaryLiteralsList(ctx.ordinaryLiteralsList()));
        }
        RGD rgd;
      
        rgd = new RGD(part_esquerra, part_dreta);
        logicSchemaRGD.addRGD(rgd);
        return rgd;
    }
    
    @Override public EGD visitEgd(LogicSchemaGrammarAmpliatParser.EgdContext ctx){
        List<Literal> body = new LinkedList(this.visitLiteralsList(ctx.literalsList()));
        BuiltInLiteral head = this.visitBuiltInLiteral(ctx.builtInLiteral());
        EGD egd;
        egd = new EGD(head, body);
        logicSchemaRGD.addEGD(egd);
        return egd;
    }
    @Override public List<List<OrdinaryLiteral>> visitOrdinaryLiteralsList(LogicSchemaGrammarAmpliatParser.OrdinaryLiteralsListContext ctx) {
        List<List<OrdinaryLiteral>> llistes_literals = new LinkedList<>();
        Iterator<LogicSchemaGrammarAmpliatParser.OrdinaryLiteralContext> it_literal = ctx.ordinaryLiteral().iterator();
        Iterator<LogicSchemaGrammarAmpliatParser.ConnectorContext> it_connector = ctx.connector().iterator();
        List<OrdinaryLiteral> literals = new LinkedList();
        if(it_literal.hasNext()) literals.add(this.visitOrdinaryLiteral(it_literal.next()));
        while(it_literal.hasNext()) {
            if (it_connector.hasNext()) {
                LogicSchemaGrammarAmpliatParser.ConnectorContext con = it_connector.next();
                if (con.getText().equals(" v ")) {
                    llistes_literals.add(literals);
                    literals = new LinkedList();
                }
            }
            literals.add(this.visitOrdinaryLiteral(it_literal.next()));
        }
        llistes_literals.add(literals);
//        for(LogicSchemaGrammarAmpliatParser.OrdinaryLiteralContext litContext: ctx.ordinaryLiteral()){
//            literals.add(this.visitOrdinaryLiteral(litContext));
//        }
        return llistes_literals;
    }
    
    @Override public List<Literal> visitLiteralsList(LogicSchemaGrammarAmpliatParser.LiteralsListContext ctx) {
        List<Literal> literals =new LinkedList();
        
        for(LogicSchemaGrammarAmpliatParser.LiteralContext litContext: ctx.literal()) {
            literals.add((Literal) this.visitLiteral(litContext));
        }
        return literals;
    }
}
