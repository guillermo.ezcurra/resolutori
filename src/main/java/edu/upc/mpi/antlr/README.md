## Instruccions ANTLR4

Des de la carpeta on hi ha la gramàtica .g4.

`antlr4 -visitor LogicSchemaGrammarAmpliat.g4`

Es crearan tots els fitxers autogenerats. Hem de compilar-los ara, sempre especificant el classpath.

`javac -classpath "/usr/share/java/antlr4-runtime.jar" *.java`

Després, si volem fer proves amb un entorn gràfic, definim un àlies per al programa de prova de l'antrl4, grun.

`alias grun='/usr/share/antlr4/grun'`

Per començar les proves, cal prémer Control+D en acabat.

`grun LogicSchemaGrammarAmpliat prog -gui`