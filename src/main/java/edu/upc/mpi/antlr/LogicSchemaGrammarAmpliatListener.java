package edu.upc.mpi.antlr;

// Generated from LogicSchemaGrammarAmpliat.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link LogicSchemaGrammarAmpliatParser}.
 */
public interface LogicSchemaGrammarAmpliatListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(LogicSchemaGrammarAmpliatParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(LogicSchemaGrammarAmpliatParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#line}.
	 * @param ctx the parse tree
	 */
	void enterLine(LogicSchemaGrammarAmpliatParser.LineContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#line}.
	 * @param ctx the parse tree
	 */
	void exitLine(LogicSchemaGrammarAmpliatParser.LineContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#constraint}.
	 * @param ctx the parse tree
	 */
	void enterConstraint(LogicSchemaGrammarAmpliatParser.ConstraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#constraint}.
	 * @param ctx the parse tree
	 */
	void exitConstraint(LogicSchemaGrammarAmpliatParser.ConstraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#derivationRule}.
	 * @param ctx the parse tree
	 */
	void enterDerivationRule(LogicSchemaGrammarAmpliatParser.DerivationRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#derivationRule}.
	 * @param ctx the parse tree
	 */
	void exitDerivationRule(LogicSchemaGrammarAmpliatParser.DerivationRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#body}.
	 * @param ctx the parse tree
	 */
	void enterBody(LogicSchemaGrammarAmpliatParser.BodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#body}.
	 * @param ctx the parse tree
	 */
	void exitBody(LogicSchemaGrammarAmpliatParser.BodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#connector}.
	 * @param ctx the parse tree
	 */
	void enterConnector(LogicSchemaGrammarAmpliatParser.ConnectorContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#connector}.
	 * @param ctx the parse tree
	 */
	void exitConnector(LogicSchemaGrammarAmpliatParser.ConnectorContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#literalsList}.
	 * @param ctx the parse tree
	 */
	void enterLiteralsList(LogicSchemaGrammarAmpliatParser.LiteralsListContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#literalsList}.
	 * @param ctx the parse tree
	 */
	void exitLiteralsList(LogicSchemaGrammarAmpliatParser.LiteralsListContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#ordinaryLiteralsList}.
	 * @param ctx the parse tree
	 */
	void enterOrdinaryLiteralsList(LogicSchemaGrammarAmpliatParser.OrdinaryLiteralsListContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#ordinaryLiteralsList}.
	 * @param ctx the parse tree
	 */
	void exitOrdinaryLiteralsList(LogicSchemaGrammarAmpliatParser.OrdinaryLiteralsListContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(LogicSchemaGrammarAmpliatParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(LogicSchemaGrammarAmpliatParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#builtInLiteral}.
	 * @param ctx the parse tree
	 */
	void enterBuiltInLiteral(LogicSchemaGrammarAmpliatParser.BuiltInLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#builtInLiteral}.
	 * @param ctx the parse tree
	 */
	void exitBuiltInLiteral(LogicSchemaGrammarAmpliatParser.BuiltInLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#ordinaryLiteral}.
	 * @param ctx the parse tree
	 */
	void enterOrdinaryLiteral(LogicSchemaGrammarAmpliatParser.OrdinaryLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#ordinaryLiteral}.
	 * @param ctx the parse tree
	 */
	void exitOrdinaryLiteral(LogicSchemaGrammarAmpliatParser.OrdinaryLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#rgd}.
	 * @param ctx the parse tree
	 */
	void enterRgd(LogicSchemaGrammarAmpliatParser.RgdContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#rgd}.
	 * @param ctx the parse tree
	 */
	void exitRgd(LogicSchemaGrammarAmpliatParser.RgdContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#egd}.
	 * @param ctx the parse tree
	 */
	void enterEgd(LogicSchemaGrammarAmpliatParser.EgdContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#egd}.
	 * @param ctx the parse tree
	 */
	void exitEgd(LogicSchemaGrammarAmpliatParser.EgdContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAtom(LogicSchemaGrammarAmpliatParser.AtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAtom(LogicSchemaGrammarAmpliatParser.AtomContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#termsList}.
	 * @param ctx the parse tree
	 */
	void enterTermsList(LogicSchemaGrammarAmpliatParser.TermsListContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#termsList}.
	 * @param ctx the parse tree
	 */
	void exitTermsList(LogicSchemaGrammarAmpliatParser.TermsListContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterPredicate(LogicSchemaGrammarAmpliatParser.PredicateContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitPredicate(LogicSchemaGrammarAmpliatParser.PredicateContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#term}.
	 * @param ctx the parse tree
	 */
	void enterTerm(LogicSchemaGrammarAmpliatParser.TermContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#term}.
	 * @param ctx the parse tree
	 */
	void exitTerm(LogicSchemaGrammarAmpliatParser.TermContext ctx);
}