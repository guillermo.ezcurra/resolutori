grammar LogicSchemaGrammarAmpliat;

NOT: 'not';
OR: ' v ';
FALSE: 'false';
OPERATOR: '='|'<>'|'<'|'>'|'<='|'>=';
ID: ([A-Za-z0-9_'#\-])+;
CONSTRAINTID: '@'[0-9]+;
NEWLINE:'\r'? '\n';
WS : [ \t]+ -> skip ; // toss out whitespace
COMMENT : '%' ~[\r\n]*;
COMMA: ',';
OPENPAR: '(';
CLOSEPAR: ')';
ARROWNORMALCLAUSE: ':-';
ARROWRGD: '->';
EXTRAINFO: '[' ~[\r\n]* ']' | '{' ~[\r\n]* '}' ;

prog: ((line)? NEWLINE)* ;
line: (COMMENT | EXTRAINFO | constraint | derivationRule | rgd | egd | ordinaryLiteral);
constraint: (CONSTRAINTID)? ARROWNORMALCLAUSE body;
derivationRule: atom ARROWNORMALCLAUSE body;
body: literalsList;
connector: COMMA | OR;
literalsList: literal (COMMA literal)*;
ordinaryLiteralsList: ordinaryLiteral (connector ordinaryLiteral)*;
literal: builtInLiteral | ordinaryLiteral;
builtInLiteral: term OPERATOR term;
ordinaryLiteral: atom | NOT OPENPAR atom CLOSEPAR;
rgd: literalsList ARROWRGD (ordinaryLiteralsList | FALSE);
egd: literalsList ARROWRGD builtInLiteral;
atom: predicate OPENPAR termsList CLOSEPAR;
termsList: | term (COMMA term)*;
predicate: ID;
term: ID;
