/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.antlr;

//import edu.upc.mpi.antlr.LogicSchemaGrammarAmpliatParser.ProgContext;
import edu.upc.mpi.logicschemaRGD.LogicSchemaRGD;
import edu.upc.mpi.antlr.LogicSchemaGrammarAmpliatParser.ProgContext;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

/**
 *
 * @author guille
 */
public class LogicSchemaAmpliatParser {
    private final String stringSchema;
    private final LogicSchemaRGD logicSchema = new LogicSchemaRGD();
    
    public LogicSchemaAmpliatParser(File file){
        this.stringSchema = this.getReadFile(file);
        assert stringSchema != null && !stringSchema.equals(""):"The given schema should not be null neither empty: "+stringSchema;
    }
    
    public LogicSchemaAmpliatParser(String schema){
        assert schema != null && !schema.equals(""):"The given schema should not be null neither empty: "+schema;
        this.stringSchema = schema;
    }
    
    protected String getReadFile(File file){
        assert file != null:"File should not be null";
        assert file.exists():"File "+file.getAbsolutePath()+" does not exists";
        
        //Reading the file
        String result = "";
        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String readLine = reader.readLine();
            while(readLine != null){
                result += readLine+"\n";
                readLine = reader.readLine();
            }
        } catch (FileNotFoundException ex) {
            assert false:ex.getMessage();
        } catch (IOException ex) {
            assert false:ex.getMessage();
        } 
        return result;
    }
    /**
     * Translates the given logic schema file or string into a LogicSchema instance
     * 
     */
    
    public void parse() {
        CharStream input = CharStreams.fromString(stringSchema);
        LogicSchemaGrammarAmpliatLexer lexer = new LogicSchemaGrammarAmpliatLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        LogicSchemaGrammarAmpliatParser parser = new LogicSchemaGrammarAmpliatParser(tokens);
        ProgContext tree =(ProgContext)parser.prog();
        LogicSchemaGrammarAmpliatVisitor visitor = new LogicSchemaGrammarAmpliatVisitorImpl(logicSchema);
        visitor.visit(tree);
    }
    
    public LogicSchemaRGD getLogicSchemaRGD(){
        return logicSchema;
    }
}
