package edu.upc.mpi.antlr;

// Generated from LogicSchemaGrammarAmpliat.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LogicSchemaGrammarAmpliatParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		NOT=1, OR=2, FALSE=3, OPERATOR=4, ID=5, CONSTRAINTID=6, NEWLINE=7, WS=8, 
		COMMENT=9, COMMA=10, OPENPAR=11, CLOSEPAR=12, ARROWNORMALCLAUSE=13, ARROWRGD=14, 
		EXTRAINFO=15;
	public static final int
		RULE_prog = 0, RULE_line = 1, RULE_constraint = 2, RULE_derivationRule = 3, 
		RULE_body = 4, RULE_connector = 5, RULE_literalsList = 6, RULE_ordinaryLiteralsList = 7, 
		RULE_literal = 8, RULE_builtInLiteral = 9, RULE_ordinaryLiteral = 10, 
		RULE_rgd = 11, RULE_egd = 12, RULE_atom = 13, RULE_termsList = 14, RULE_predicate = 15, 
		RULE_term = 16;
	private static String[] makeRuleNames() {
		return new String[] {
			"prog", "line", "constraint", "derivationRule", "body", "connector", 
			"literalsList", "ordinaryLiteralsList", "literal", "builtInLiteral", 
			"ordinaryLiteral", "rgd", "egd", "atom", "termsList", "predicate", "term"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'not'", "' v '", "'false'", null, null, null, null, null, null, 
			"','", "'('", "')'", "':-'", "'->'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "NOT", "OR", "FALSE", "OPERATOR", "ID", "CONSTRAINTID", "NEWLINE", 
			"WS", "COMMENT", "COMMA", "OPENPAR", "CLOSEPAR", "ARROWNORMALCLAUSE", 
			"ARROWRGD", "EXTRAINFO"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "LogicSchemaGrammarAmpliat.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public LogicSchemaGrammarAmpliatParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgContext extends ParserRuleContext {
		public List<TerminalNode> NEWLINE() { return getTokens(LogicSchemaGrammarAmpliatParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(LogicSchemaGrammarAmpliatParser.NEWLINE, i);
		}
		public List<LineContext> line() {
			return getRuleContexts(LineContext.class);
		}
		public LineContext line(int i) {
			return getRuleContext(LineContext.class,i);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterProg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitProg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitProg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(40);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NOT) | (1L << ID) | (1L << CONSTRAINTID) | (1L << NEWLINE) | (1L << COMMENT) | (1L << ARROWNORMALCLAUSE) | (1L << EXTRAINFO))) != 0)) {
				{
				{
				setState(35);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NOT) | (1L << ID) | (1L << CONSTRAINTID) | (1L << COMMENT) | (1L << ARROWNORMALCLAUSE) | (1L << EXTRAINFO))) != 0)) {
					{
					setState(34);
					line();
					}
				}

				setState(37);
				match(NEWLINE);
				}
				}
				setState(42);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LineContext extends ParserRuleContext {
		public TerminalNode COMMENT() { return getToken(LogicSchemaGrammarAmpliatParser.COMMENT, 0); }
		public TerminalNode EXTRAINFO() { return getToken(LogicSchemaGrammarAmpliatParser.EXTRAINFO, 0); }
		public ConstraintContext constraint() {
			return getRuleContext(ConstraintContext.class,0);
		}
		public DerivationRuleContext derivationRule() {
			return getRuleContext(DerivationRuleContext.class,0);
		}
		public RgdContext rgd() {
			return getRuleContext(RgdContext.class,0);
		}
		public EgdContext egd() {
			return getRuleContext(EgdContext.class,0);
		}
		public OrdinaryLiteralContext ordinaryLiteral() {
			return getRuleContext(OrdinaryLiteralContext.class,0);
		}
		public LineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_line; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitLine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitLine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LineContext line() throws RecognitionException {
		LineContext _localctx = new LineContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_line);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(50);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				{
				setState(43);
				match(COMMENT);
				}
				break;
			case 2:
				{
				setState(44);
				match(EXTRAINFO);
				}
				break;
			case 3:
				{
				setState(45);
				constraint();
				}
				break;
			case 4:
				{
				setState(46);
				derivationRule();
				}
				break;
			case 5:
				{
				setState(47);
				rgd();
				}
				break;
			case 6:
				{
				setState(48);
				egd();
				}
				break;
			case 7:
				{
				setState(49);
				ordinaryLiteral();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstraintContext extends ParserRuleContext {
		public TerminalNode ARROWNORMALCLAUSE() { return getToken(LogicSchemaGrammarAmpliatParser.ARROWNORMALCLAUSE, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public TerminalNode CONSTRAINTID() { return getToken(LogicSchemaGrammarAmpliatParser.CONSTRAINTID, 0); }
		public ConstraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterConstraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitConstraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitConstraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstraintContext constraint() throws RecognitionException {
		ConstraintContext _localctx = new ConstraintContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_constraint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(53);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CONSTRAINTID) {
				{
				setState(52);
				match(CONSTRAINTID);
				}
			}

			setState(55);
			match(ARROWNORMALCLAUSE);
			setState(56);
			body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DerivationRuleContext extends ParserRuleContext {
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public TerminalNode ARROWNORMALCLAUSE() { return getToken(LogicSchemaGrammarAmpliatParser.ARROWNORMALCLAUSE, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public DerivationRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_derivationRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterDerivationRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitDerivationRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitDerivationRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DerivationRuleContext derivationRule() throws RecognitionException {
		DerivationRuleContext _localctx = new DerivationRuleContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_derivationRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(58);
			atom();
			setState(59);
			match(ARROWNORMALCLAUSE);
			setState(60);
			body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyContext extends ParserRuleContext {
		public LiteralsListContext literalsList() {
			return getRuleContext(LiteralsListContext.class,0);
		}
		public BodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BodyContext body() throws RecognitionException {
		BodyContext _localctx = new BodyContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_body);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(62);
			literalsList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConnectorContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(LogicSchemaGrammarAmpliatParser.COMMA, 0); }
		public TerminalNode OR() { return getToken(LogicSchemaGrammarAmpliatParser.OR, 0); }
		public ConnectorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_connector; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterConnector(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitConnector(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitConnector(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConnectorContext connector() throws RecognitionException {
		ConnectorContext _localctx = new ConnectorContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_connector);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(64);
			_la = _input.LA(1);
			if ( !(_la==OR || _la==COMMA) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralsListContext extends ParserRuleContext {
		public List<LiteralContext> literal() {
			return getRuleContexts(LiteralContext.class);
		}
		public LiteralContext literal(int i) {
			return getRuleContext(LiteralContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(LogicSchemaGrammarAmpliatParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(LogicSchemaGrammarAmpliatParser.COMMA, i);
		}
		public LiteralsListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literalsList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterLiteralsList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitLiteralsList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitLiteralsList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralsListContext literalsList() throws RecognitionException {
		LiteralsListContext _localctx = new LiteralsListContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_literalsList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(66);
			literal();
			setState(71);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(67);
				match(COMMA);
				setState(68);
				literal();
				}
				}
				setState(73);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrdinaryLiteralsListContext extends ParserRuleContext {
		public List<OrdinaryLiteralContext> ordinaryLiteral() {
			return getRuleContexts(OrdinaryLiteralContext.class);
		}
		public OrdinaryLiteralContext ordinaryLiteral(int i) {
			return getRuleContext(OrdinaryLiteralContext.class,i);
		}
		public List<ConnectorContext> connector() {
			return getRuleContexts(ConnectorContext.class);
		}
		public ConnectorContext connector(int i) {
			return getRuleContext(ConnectorContext.class,i);
		}
		public OrdinaryLiteralsListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ordinaryLiteralsList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterOrdinaryLiteralsList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitOrdinaryLiteralsList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitOrdinaryLiteralsList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrdinaryLiteralsListContext ordinaryLiteralsList() throws RecognitionException {
		OrdinaryLiteralsListContext _localctx = new OrdinaryLiteralsListContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_ordinaryLiteralsList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(74);
			ordinaryLiteral();
			setState(80);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==OR || _la==COMMA) {
				{
				{
				setState(75);
				connector();
				setState(76);
				ordinaryLiteral();
				}
				}
				setState(82);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public BuiltInLiteralContext builtInLiteral() {
			return getRuleContext(BuiltInLiteralContext.class,0);
		}
		public OrdinaryLiteralContext ordinaryLiteral() {
			return getRuleContext(OrdinaryLiteralContext.class,0);
		}
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_literal);
		try {
			setState(85);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(83);
				builtInLiteral();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(84);
				ordinaryLiteral();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BuiltInLiteralContext extends ParserRuleContext {
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public TerminalNode OPERATOR() { return getToken(LogicSchemaGrammarAmpliatParser.OPERATOR, 0); }
		public BuiltInLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtInLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterBuiltInLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitBuiltInLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitBuiltInLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BuiltInLiteralContext builtInLiteral() throws RecognitionException {
		BuiltInLiteralContext _localctx = new BuiltInLiteralContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_builtInLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(87);
			term();
			setState(88);
			match(OPERATOR);
			setState(89);
			term();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrdinaryLiteralContext extends ParserRuleContext {
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public TerminalNode NOT() { return getToken(LogicSchemaGrammarAmpliatParser.NOT, 0); }
		public TerminalNode OPENPAR() { return getToken(LogicSchemaGrammarAmpliatParser.OPENPAR, 0); }
		public TerminalNode CLOSEPAR() { return getToken(LogicSchemaGrammarAmpliatParser.CLOSEPAR, 0); }
		public OrdinaryLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ordinaryLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterOrdinaryLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitOrdinaryLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitOrdinaryLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrdinaryLiteralContext ordinaryLiteral() throws RecognitionException {
		OrdinaryLiteralContext _localctx = new OrdinaryLiteralContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_ordinaryLiteral);
		try {
			setState(97);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(91);
				atom();
				}
				break;
			case NOT:
				enterOuterAlt(_localctx, 2);
				{
				setState(92);
				match(NOT);
				setState(93);
				match(OPENPAR);
				setState(94);
				atom();
				setState(95);
				match(CLOSEPAR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RgdContext extends ParserRuleContext {
		public LiteralsListContext literalsList() {
			return getRuleContext(LiteralsListContext.class,0);
		}
		public TerminalNode ARROWRGD() { return getToken(LogicSchemaGrammarAmpliatParser.ARROWRGD, 0); }
		public OrdinaryLiteralsListContext ordinaryLiteralsList() {
			return getRuleContext(OrdinaryLiteralsListContext.class,0);
		}
		public TerminalNode FALSE() { return getToken(LogicSchemaGrammarAmpliatParser.FALSE, 0); }
		public RgdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rgd; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterRgd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitRgd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitRgd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RgdContext rgd() throws RecognitionException {
		RgdContext _localctx = new RgdContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_rgd);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(99);
			literalsList();
			setState(100);
			match(ARROWRGD);
			setState(103);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NOT:
			case ID:
				{
				setState(101);
				ordinaryLiteralsList();
				}
				break;
			case FALSE:
				{
				setState(102);
				match(FALSE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EgdContext extends ParserRuleContext {
		public LiteralsListContext literalsList() {
			return getRuleContext(LiteralsListContext.class,0);
		}
		public TerminalNode ARROWRGD() { return getToken(LogicSchemaGrammarAmpliatParser.ARROWRGD, 0); }
		public BuiltInLiteralContext builtInLiteral() {
			return getRuleContext(BuiltInLiteralContext.class,0);
		}
		public EgdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_egd; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterEgd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitEgd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitEgd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EgdContext egd() throws RecognitionException {
		EgdContext _localctx = new EgdContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_egd);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(105);
			literalsList();
			setState(106);
			match(ARROWRGD);
			setState(107);
			builtInLiteral();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomContext extends ParserRuleContext {
		public PredicateContext predicate() {
			return getRuleContext(PredicateContext.class,0);
		}
		public TerminalNode OPENPAR() { return getToken(LogicSchemaGrammarAmpliatParser.OPENPAR, 0); }
		public TermsListContext termsList() {
			return getRuleContext(TermsListContext.class,0);
		}
		public TerminalNode CLOSEPAR() { return getToken(LogicSchemaGrammarAmpliatParser.CLOSEPAR, 0); }
		public AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitAtom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitAtom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtomContext atom() throws RecognitionException {
		AtomContext _localctx = new AtomContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_atom);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(109);
			predicate();
			setState(110);
			match(OPENPAR);
			setState(111);
			termsList();
			setState(112);
			match(CLOSEPAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermsListContext extends ParserRuleContext {
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(LogicSchemaGrammarAmpliatParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(LogicSchemaGrammarAmpliatParser.COMMA, i);
		}
		public TermsListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_termsList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterTermsList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitTermsList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitTermsList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TermsListContext termsList() throws RecognitionException {
		TermsListContext _localctx = new TermsListContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_termsList);
		int _la;
		try {
			setState(123);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CLOSEPAR:
				enterOuterAlt(_localctx, 1);
				{
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(115);
				term();
				setState(120);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(116);
					match(COMMA);
					setState(117);
					term();
					}
					}
					setState(122);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredicateContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(LogicSchemaGrammarAmpliatParser.ID, 0); }
		public PredicateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterPredicate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitPredicate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitPredicate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PredicateContext predicate() throws RecognitionException {
		PredicateContext _localctx = new PredicateContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_predicate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(125);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(LogicSchemaGrammarAmpliatParser.ID, 0); }
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).enterTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LogicSchemaGrammarAmpliatListener ) ((LogicSchemaGrammarAmpliatListener)listener).exitTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicSchemaGrammarAmpliatVisitor ) return ((LogicSchemaGrammarAmpliatVisitor<? extends T>)visitor).visitTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_term);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(127);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\21\u0084\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\3\2\5\2&\n\2\3\2\7\2)\n\2\f\2\16\2,\13\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\5\3\65\n\3\3\4\5\48\n\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\6\3\6\3\7\3\7\3"+
		"\b\3\b\3\b\7\bH\n\b\f\b\16\bK\13\b\3\t\3\t\3\t\3\t\7\tQ\n\t\f\t\16\tT"+
		"\13\t\3\n\3\n\5\nX\n\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\5\f"+
		"d\n\f\3\r\3\r\3\r\3\r\5\rj\n\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17"+
		"\3\17\3\20\3\20\3\20\3\20\7\20y\n\20\f\20\16\20|\13\20\5\20~\n\20\3\21"+
		"\3\21\3\22\3\22\3\22\2\2\23\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \""+
		"\2\3\4\2\4\4\f\f\2\u0082\2*\3\2\2\2\4\64\3\2\2\2\6\67\3\2\2\2\b<\3\2\2"+
		"\2\n@\3\2\2\2\fB\3\2\2\2\16D\3\2\2\2\20L\3\2\2\2\22W\3\2\2\2\24Y\3\2\2"+
		"\2\26c\3\2\2\2\30e\3\2\2\2\32k\3\2\2\2\34o\3\2\2\2\36}\3\2\2\2 \177\3"+
		"\2\2\2\"\u0081\3\2\2\2$&\5\4\3\2%$\3\2\2\2%&\3\2\2\2&\'\3\2\2\2\')\7\t"+
		"\2\2(%\3\2\2\2),\3\2\2\2*(\3\2\2\2*+\3\2\2\2+\3\3\2\2\2,*\3\2\2\2-\65"+
		"\7\13\2\2.\65\7\21\2\2/\65\5\6\4\2\60\65\5\b\5\2\61\65\5\30\r\2\62\65"+
		"\5\32\16\2\63\65\5\26\f\2\64-\3\2\2\2\64.\3\2\2\2\64/\3\2\2\2\64\60\3"+
		"\2\2\2\64\61\3\2\2\2\64\62\3\2\2\2\64\63\3\2\2\2\65\5\3\2\2\2\668\7\b"+
		"\2\2\67\66\3\2\2\2\678\3\2\2\289\3\2\2\29:\7\17\2\2:;\5\n\6\2;\7\3\2\2"+
		"\2<=\5\34\17\2=>\7\17\2\2>?\5\n\6\2?\t\3\2\2\2@A\5\16\b\2A\13\3\2\2\2"+
		"BC\t\2\2\2C\r\3\2\2\2DI\5\22\n\2EF\7\f\2\2FH\5\22\n\2GE\3\2\2\2HK\3\2"+
		"\2\2IG\3\2\2\2IJ\3\2\2\2J\17\3\2\2\2KI\3\2\2\2LR\5\26\f\2MN\5\f\7\2NO"+
		"\5\26\f\2OQ\3\2\2\2PM\3\2\2\2QT\3\2\2\2RP\3\2\2\2RS\3\2\2\2S\21\3\2\2"+
		"\2TR\3\2\2\2UX\5\24\13\2VX\5\26\f\2WU\3\2\2\2WV\3\2\2\2X\23\3\2\2\2YZ"+
		"\5\"\22\2Z[\7\6\2\2[\\\5\"\22\2\\\25\3\2\2\2]d\5\34\17\2^_\7\3\2\2_`\7"+
		"\r\2\2`a\5\34\17\2ab\7\16\2\2bd\3\2\2\2c]\3\2\2\2c^\3\2\2\2d\27\3\2\2"+
		"\2ef\5\16\b\2fi\7\20\2\2gj\5\20\t\2hj\7\5\2\2ig\3\2\2\2ih\3\2\2\2j\31"+
		"\3\2\2\2kl\5\16\b\2lm\7\20\2\2mn\5\24\13\2n\33\3\2\2\2op\5 \21\2pq\7\r"+
		"\2\2qr\5\36\20\2rs\7\16\2\2s\35\3\2\2\2t~\3\2\2\2uz\5\"\22\2vw\7\f\2\2"+
		"wy\5\"\22\2xv\3\2\2\2y|\3\2\2\2zx\3\2\2\2z{\3\2\2\2{~\3\2\2\2|z\3\2\2"+
		"\2}t\3\2\2\2}u\3\2\2\2~\37\3\2\2\2\177\u0080\7\7\2\2\u0080!\3\2\2\2\u0081"+
		"\u0082\7\7\2\2\u0082#\3\2\2\2\r%*\64\67IRWciz}";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}