package edu.upc.mpi.antlr;

// Generated from LogicSchemaGrammarAmpliat.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link LogicSchemaGrammarAmpliatParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface LogicSchemaGrammarAmpliatVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#prog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProg(LogicSchemaGrammarAmpliatParser.ProgContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLine(LogicSchemaGrammarAmpliatParser.LineContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstraint(LogicSchemaGrammarAmpliatParser.ConstraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#derivationRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDerivationRule(LogicSchemaGrammarAmpliatParser.DerivationRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody(LogicSchemaGrammarAmpliatParser.BodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#connector}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConnector(LogicSchemaGrammarAmpliatParser.ConnectorContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#literalsList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteralsList(LogicSchemaGrammarAmpliatParser.LiteralsListContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#ordinaryLiteralsList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrdinaryLiteralsList(LogicSchemaGrammarAmpliatParser.OrdinaryLiteralsListContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(LogicSchemaGrammarAmpliatParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#builtInLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuiltInLiteral(LogicSchemaGrammarAmpliatParser.BuiltInLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#ordinaryLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrdinaryLiteral(LogicSchemaGrammarAmpliatParser.OrdinaryLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#rgd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRgd(LogicSchemaGrammarAmpliatParser.RgdContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#egd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEgd(LogicSchemaGrammarAmpliatParser.EgdContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtom(LogicSchemaGrammarAmpliatParser.AtomContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#termsList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTermsList(LogicSchemaGrammarAmpliatParser.TermsListContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#predicate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicate(LogicSchemaGrammarAmpliatParser.PredicateContext ctx);
	/**
	 * Visit a parse tree produced by {@link LogicSchemaGrammarAmpliatParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTerm(LogicSchemaGrammarAmpliatParser.TermContext ctx);
}