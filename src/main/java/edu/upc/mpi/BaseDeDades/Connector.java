/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.BaseDeDades;

import edu.upc.mpi.antlr.LogicSchemaAmpliatParser;
import edu.upc.mpi.logicschema.Atom;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschema.Predicate;
import edu.upc.mpi.logicschema.PredicateImpl;
import edu.upc.mpi.logicschema.Term;
import edu.upc.mpi.logicschemaRGD.LabelledNull;
import edu.upc.mpi.logicschemaRGD.LogicSchemaRGD;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author guille
 */
public class Connector {
    private static final String dbClassName = "com.mysql.cj.jdbc.Driver";

    private static String URL;
    private static String USUARI;
    private static String CLAU;
    
    
    
    
    public static void main(String[] args) throws Exception {
        Connector n = new Connector("jdbc:mysql://localhost:3306/Llibreria","root","Prova.123");
        //List<OrdinaryLiteral> fetsEntrada = n.escriureFets();
        //insercio(fetsEntrada);
        
        List<OrdinaryLiteral> fetsTaula = n.obtenirTuplesBD("Llibreria");
        for (OrdinaryLiteral l : fetsTaula)  System.out.println(l.toString());
        

    }  

    public Connector(String url, String nom_usuari, String clau) {
        URL = url;
        USUARI = nom_usuari;
        CLAU = clau;
    }
    
    
    public static Connection getConnection() throws Exception{
        try{
            Class.forName(dbClassName);
            byte[] decodedBytes = Base64.getDecoder().decode(CLAU);
            String clauDecodificada = new String(decodedBytes);
            Connection conn = DriverManager.getConnection(URL, USUARI,clauDecodificada);
            System.out.println("Connectat correctament");
            return conn;
        } catch(Exception e){System.out.println(e);}
        return null;
    }
//    /**
//     * Funció de connexió a una BD passant per paràmetres tot el que cal per connectar-s'hi.
//     * @return La connexió creada
//     * @throws Exception 
//     */
//    public static Connection getConnection(String controlador, String url, String nom_usuari, String clau) throws Exception{
//        try{
//            Class.forName(controlador);
//            
//            Connection conn = DriverManager.getConnection(url, nom_usuari,clau);
//            System.out.println("Connectat correctament");
//            return conn;
//        } catch(Exception e){System.out.println(e);}
//        return null;
//    }
    
    public static void createTable() throws Exception{
        try{
            Connection con = getConnection();
            //Aquí cal enviar els paràmetres per crear la taula amb el predicat com a table name i els termes com a atributs.
            PreparedStatement create = con.prepareStatement("CREATE TABLE IF NOT EXISTS tablename(id int NOT NULL AUTO_INCREMENT, first varchar(255), last varchar(255), PRIMARY KEY(id))");
            
            create.executeUpdate();
            
            
        }catch(Exception e){System.out.println(e);}
        finally{
            System.out.println("Funció acabada");
        }
    }
    
    public static void insercio(List<OrdinaryLiteral> nousLiterals) throws Exception {
        try{
            Connection con = getConnection();
            for(OrdinaryLiteral f : nousLiterals) {
                List<String> cols = obtenirNomColumnes(f,con);
                String columnes = formatar_columnes(cols);
                String parametres = formatar_parametres(f.getTerms());
                PreparedStatement novatupla = con.prepareStatement("INSERT INTO "+ f.getPredicateName() + columnes + " VALUES ("+ parametres +")");
                novatupla.executeUpdate();
            }
        } catch(Exception e){System.out.println(e);}
        finally {
            System.out.println("Inserció completada");
        }
    }
    
    public static void esborrament(List<OrdinaryLiteral> literalsAEsborrar) throws Exception {
        try{
            //TODO: fer funcions esborrar
            Connection con = getConnection();
            for(OrdinaryLiteral f : literalsAEsborrar) {
                List<String> cols = obtenirNomColumnes(f,con);
                
                String parametres = formatar_parametres(f.getTerms());
                //PreparedStatement novatupla = con.prepareStatement("INSERT INTO "+ f.getPredicateName() + columnes + " VALUES ("+ parametres +")");
                //novatupla.executeUpdate();
            }
        } catch(Exception e){System.out.println(e);}
        finally {
            System.out.println("Inserció completada");
        }
    }   
    
    
    public static List<String> obtenirNomColumnes(OrdinaryLiteral f, Connection con) throws Exception {
        try{
            PreparedStatement columnes = con.prepareStatement("select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='"+f.getPredicateName()+"' ORDER BY ORDINAL_POSITION");
            
            ResultSet result = columnes.executeQuery();
            List<String> cols = new LinkedList<>();
            while(result.next()) {
                String resultat = result.getString("COLUMN_NAME");
                cols.add(resultat);
                System.out.println(resultat);
            }
            return cols;
        } catch(Exception e){System.out.println(e);}
        return null;
    }
    
    public static List<String> obtenirNomsTaules(String nomBD) throws Exception {
        try {
            Connection con = getConnection();
            List<String> nomsTaules = new LinkedList<>();
            PreparedStatement taules = con.prepareStatement("SELECT table_name FROM information_schema.tables WHERE table_schema = '"+ nomBD +"';");
            ResultSet result = taules.executeQuery();
            while(result.next()) {
                nomsTaules.add(result.getString("TABLE_NAME"));
            }
            return nomsTaules;
        } catch(Exception e){System.out.println(e);}
        return null;
    }
    
    public static List<OrdinaryLiteral> obtenirTuplesTaula(String predicat, Connection con) throws Exception {
        try{
            List<OrdinaryLiteral> literals = new LinkedList<>();
            PreparedStatement columnes = con.prepareStatement("select * from " + predicat);
            
            ResultSet result = columnes.executeQuery();
            ResultSetMetaData rsmd = result.getMetaData();
            int nombrecolumnes = rsmd.getColumnCount();
//            System.out.println(result.);
            List<Term> termes = new LinkedList<>();
            while(result.next()) {
                termes = new LinkedList<>();
                for (int i = 1; i <= nombrecolumnes; i++) {
                    String columnValue = result.getString(i);
                    Term t;
                    if(isNumeric(columnValue))t = new Term(columnValue);
                    else {
                        if(columnValue == null) t = new LabelledNull();
                        else t = new Term("'"+columnValue+"'");
                    }
                    termes.add(t);
                }
                
                OrdinaryLiteral ol = new OrdinaryLiteral(new Atom(new PredicateImpl(predicat,rsmd.getColumnCount()),termes),true);
                literals.add(ol);
            }
            return literals;
        } catch(Exception e){System.out.println(e);}
        return null;
    }
    

    public static List<OrdinaryLiteral> obtenirTuplesBD(String nomBD) throws Exception {
        try{
            Connection con = getConnection();
            List<String> Taules = obtenirNomsTaules(nomBD);
            List<OrdinaryLiteral> literals = new LinkedList<>();
            for(String t : Taules) literals.addAll(obtenirTuplesTaula(t,con));
            //LabelledNull.resetComptador();
            return literals;
        } catch(Exception e){System.out.println(e);}
        return null;
    }
    
    public static String formatar_columnes(List<String> cols) {
        String columnes = new String();
        columnes += "(";
        int it = 0;
        for (String col: cols) {
            if (it == cols.size() - 1) columnes += col + ")";
            else {
                columnes += col + ", ";
            }
            it++;
        }
        return columnes;
    }
    public static String formatar_parametres(List<Term> termes) {
        String parametres = new String();
        int it = 0;
        for (Term t: termes) {
            if (it == termes.size() - 1) parametres += t.toString();
            else {
                parametres += t.toString() + ", ";
            }
            it++;
        }
        return parametres;
    }
    
    public static List<OrdinaryLiteral> escriureFets() {
        String fets_entrada = new String();
        Scanner scan = new Scanner(System.in);
        System.out.println("Escriu els fets inicials aquí, escriu prou quan hagis acabat");   
        while (scan.hasNextLine()) {
            String linia = scan.nextLine();
            if (!linia.equals("prou")) {
                fets_entrada += linia + "\n";
            }
            else break;
        } 
        LogicSchemaAmpliatParser instance1 = new LogicSchemaAmpliatParser(fets_entrada);
        instance1.parse();
        LogicSchemaRGD iniciLogicSchemaRGD = instance1.getLogicSchemaRGD();
        List<OrdinaryLiteral> fetsEntrada = new LinkedList<>();
        fetsEntrada.addAll(iniciLogicSchemaRGD.getAllFetsEntrada());
        return fetsEntrada;
    }
    
    public static boolean isNumeric(String strNum) {
	    if (strNum == null) {
	        return false;
	    }
	    try {
	        double d = Double.parseDouble(strNum);
	    } catch (NumberFormatException nfe) {
	        return false;
	    }
	    return true;
	}
}
