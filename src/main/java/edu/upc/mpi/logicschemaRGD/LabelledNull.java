/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.logicschemaRGD;

import edu.upc.mpi.logicschema.Term;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author guille
 */
public class LabelledNull extends Term{
    private static int nextID = 0;                    
    
    public LabelledNull() {
            super("#"+String.valueOf(nextID));
            nextID++;
    }
    
    /**
     * Comprova si el terme és un labelled null
     * @return 
     */
    public boolean isLabelledNull() {
        if(this.getName().startsWith("#"))  {
            return true;
        }
        else return false;
    }
    
    @Override
    public boolean isVariable() {
        return !isConstant() && !isLabelledNull();
    }
    
    @Override
    public boolean nameIsModifiable() {
        return true;
    }
    
    public static void resetComptador() {
        nextID = 0;
    }
    
    
    /**
     * Returns a copy of this term after applying the given substitution, if
     * substitution is not null. Otherwise, it just returns a copy of this.
     *
     * @param substitution
     * @return 
     */
    @Override
    public Term getSubstitutedTerm(Map<String, String> substitution) {
        if (substitution != null && substitution.containsKey(this.getName())) {
            return new Term(substitution.get(this.getName()));
        }else return this.copy();
    }
    /**
     * @param thatVariable != null
     * @param substitution != null
     * @return a new substitution containing the given substitution, but possibly adding
     * a replacement from this variable to thatVariable. If thatVariable is not a variable
     * (whereas this is) or such substitution does not exists, it returns null.
     * If this and thatVariable are constants with the same value, it returns
     * a copy of the given substitution.
     */
    @Override
    protected Map<String, String> getVariableToVariableUnification(Term thatVariable, Map<String, String> substitution) {  
        assert thatVariable != null:"thatVariable cannot be null";
        assert substitution != null:"substitution cannot be null";
        
        Map<String, String> result = new HashMap(substitution);
        if(result.containsKey(this.getName())){
            assert this.isVariable():"The substitution should not map the constant term: "+this.getName();
            String thisSubstituted = substitution.get(this.getName());
            if(thatVariable.getName().equals(thisSubstituted)) return result;
            else return null;
        } else {
            if(this.isConstant()) {
                return this.getName().equals(thatVariable.getName())? result: null;
            } else {
                if(thatVariable.isVariable()){
                    result.put(this.getName(), thatVariable.getName());
                    return result;
                } else return null;
            }
        }
    }

    private boolean isValidName(String name) {
        List<Character> forbiddenSymbols = new LinkedList();
        forbiddenSymbols.add(',');
        forbiddenSymbols.add('(');
        forbiddenSymbols.add(')');
        forbiddenSymbols.add('%');
        
        for(char c: name.toCharArray()){
            if(forbiddenSymbols.contains(c)) return false;
        }
        
        return true;
    }
}
    

