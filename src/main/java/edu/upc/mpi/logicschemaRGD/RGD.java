/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.logicschemaRGD;

import edu.upc.mpi.chase.UtilsChase;
import edu.upc.mpi.logicschema.BuiltInLiteral;
import edu.upc.mpi.logicschema.Literal;
import edu.upc.mpi.logicschema.NormalClause;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschema.Term;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Implementació d'una Repair Generating Dependency
 * @author Guillermo Ezcurra
 */
public class RGD extends NormalClause{
    private final List<List<Literal>> partDreta;
    
    /**
     * 
     * @param partEsquerra
     * @param partsDretes
     */

    
    public RGD(List<Literal> partEsquerra, List<List<Literal>> partsDretes){
        super(partEsquerra);
        assert partsDretes != null: "No es pot creat una RGD amb la part dreta buida.";
        assert partsDretes.size() > 0: "No es pot creat una RGD amb la part dreta buida";
        for(List<Literal> lit_list: partsDretes) {
            for(Literal lit: lit_list) assert lit!=null;
        }
        this.partDreta = partsDretes;
    }
    
    
    public List<Literal> getPartDreta(int i) {
        if(!partDreta.isEmpty()) return partDreta.get(i);
        else return null;
    }
    
    public List<List<Literal>> getPartsDretes() {
        return this.partDreta;
    }
    
    public boolean teDisjuncions() {
        return (this.partDreta.size() > 1);
    }
    
    public int nombreDeDisjuncions() {
        return this.partDreta.size();
    }
    /**
     * Mirem si l'RGD té paràmetres a la part dreta que no apareixen a la part Esquerra de la regla
     * @return 
     */
    public boolean teExistencials() {
        boolean exist = false;
        List<Term> TermesEsquerra = new LinkedList<>();
        List<Term> TermesDreta = new LinkedList<>();
        //Carreguem tots els termes dels literals de part esquerra a Termes Esquerra
        for (Literal l: this.getLiterals()) {
            OrdinaryLiteral l_aux = (OrdinaryLiteral) l;
            TermesEsquerra.addAll(l_aux.getAtom().getTerms());
        }
        //Carreguem tots els termes dels literals de part dreta a Termes Dreta
        for(List<Literal> llista_dretes : this.getPartsDretes()) {
            for (Literal l: llista_dretes) {
            OrdinaryLiteral l_aux = (OrdinaryLiteral) l;
            TermesDreta.addAll(l_aux.getAtom().getTerms());
            }
            for (Term t : TermesDreta) {
                if(!TermesEsquerra.contains(t)){
                    exist = true;
                    break;
                }
            }
            if (exist) break;
        }
        
        return exist;
    }
     /**
     * Mirem si l'RGD té paràmetres a la part dreta que no apareixen a la part Esquerra de la regla
     * @return 
     */
    public boolean teExistencialsPart(List<Literal> TermesD) {
        boolean exist = false;
        List<Term> TermesEsquerra = new LinkedList<>();
        List<Term> TermesDreta = new LinkedList<>();
        //Carreguem tots els termes dels literals de part esquerra a Termes Esquerra
        for (Literal l: this.getLiterals()) {
            OrdinaryLiteral l_aux = (OrdinaryLiteral) l;
            TermesEsquerra.addAll(l_aux.getAtom().getTerms());
        }
        //Carreguem tots els termes dels literals de part dreta a Termes Dreta
        
        for (Literal l: TermesD) {
            OrdinaryLiteral l_aux = (OrdinaryLiteral) l;
            TermesDreta.addAll(l_aux.getAtom().getTerms());
        }
        for (Term t : TermesDreta) {
            if(!TermesEsquerra.contains(t)){
                exist = true;
                break;
            }
        }
        
        
        return exist;
    }
    
    @Override
    public String toString()
    {
        
        String result = new String();
        boolean commaRequired=false;
        for(Literal lit: super.getLiterals()){
            if(commaRequired)result+=", ";
            result+=lit.toString();
            commaRequired=true;
        }
        result += " -> ";
        commaRequired=false;
        int it = 0;
        if(partDreta.isEmpty()) {
            result += " false ";
        }
        else {
            for(List<Literal> lit_list : partDreta) {
               if(it != 0) {
                   result += " v ";
                   commaRequired=false;
               }
               for(Literal lit: lit_list){
                if(commaRequired)result+=", ";
                result+=lit.toString();
                commaRequired=true;
               }
               it++;
            }
        }
        return result;
    }
    
    @Override
    protected String getHeadAsString() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected int hashCodeSpecific() {
        return Objects.hashCode(this.partDreta);
    }
    
    
    public List<Literal> getUnifiableLiterals(Literal thatLiteral, Map<String, String> substitution) {
        List<Literal> result = new LinkedList<Literal>();
        for(Literal thisLiteral: this.getLiterals()){
            if(thatLiteral.isVariableUnifiable(thisLiteral, substitution)) result.add(thisLiteral);
        }
        return result;
    }


    @Override
    protected Map<String, String> getVariableToVariableSubstitutionForHead(NormalClause nc) {
        if (nc instanceof RGD) {
            RGD r = (RGD) nc;
            Map<String, String> substitution1 = new HashMap<>();
            //això hauria de funcionar
            Map<String, String> substitution = new HashMap<>();
            if(!(this.partDreta.isEmpty() || r.getPartsDretes().isEmpty())){
                substitution = RGD.getVariableToVariableSubstitutionForLiterals(substitution1, this.partDreta.get(0), r.getPartDreta(0));
            }
            else substitution = null;
            //Checking that unsafe variables are still unsafe
            if(substitution != null){
                for(Map.Entry<String, String> entry: substitution.entrySet()){
                    String originalVariable = entry.getKey();
                    if(!this.getSafeVariablesNames().contains(originalVariable)){
                        String mappedVariable = entry.getValue();
                        if(nc.getSafeVariablesNames().contains(mappedVariable)){
                            return null;
                        }
                    }
                }
            }

            return substitution;
        }
        else return null;
    }
    
    
    @Override
    public NormalClause copyChangingBody(List<Literal> list) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    /**
     * S'apliquen les substitucions del paràmetre substitucions als OrdinaryLiterals de la part Dreta
     * @param substitucions
     * @param fetsTotals
     * @return 
     */
    public List<OrdinaryLiteral> aplicarSubstitucions(List<Map<String,String>> substitucions, List<OrdinaryLiteral> fetsTotals, int i) {
        List<OrdinaryLiteral> nousFetsRegla = new LinkedList<>();
        boolean existencial = this.teExistencials();
        for (Literal l : this.getPartDreta(i)) {
            for(Map<String,String> sub : substitucions) {
                OrdinaryLiteral l_or = (OrdinaryLiteral) l;
                if(existencial) {
                    OrdinaryLiteral nouFet = (OrdinaryLiteral) l.getLiteralAfterSubstitution(sub);
                    if(!UtilsChase.son_constants_o_null(nouFet.getTerms())) {
                        nouFet = UtilsChase.substituirVariablesPerNulls(nouFet);
                        if(!UtilsChase.existeixAmbNulls(nouFet, fetsTotals)) {
                            nousFetsRegla.add(nouFet);
                        }
                    }
                    else if(!UtilsChase.existeix(l_or, sub, fetsTotals)) {
                        nousFetsRegla.add(nouFet);
                    }
                }
                else {
                    if(!UtilsChase.existeix(l_or, sub, fetsTotals) && UtilsChase.son_constants_o_null(l_or.getLiteralAfterSubstitution(sub).getTerms())) {
                        OrdinaryLiteral nouFet = (OrdinaryLiteral) l.getLiteralAfterSubstitution(sub);
                        nousFetsRegla.add(nouFet);
                    } 
                }

            }
            fetsTotals = UtilsChase.afegirLiterals(fetsTotals, nousFetsRegla);
        }
        return nousFetsRegla;
    }
    
    public List<List<OrdinaryLiteral>> aplicarSubstitucionsDisjuncio(List<Map<String,String>> substitucions, List<OrdinaryLiteral> fetsTotals) {
        List<OrdinaryLiteral> fetsTotalsISubstituts = new LinkedList<>(fetsTotals);
        List<List<OrdinaryLiteral>> possiblesSubstituts = new ArrayList<>();
        for(int i = 0; i < this.partDreta.size(); i++) {
            possiblesSubstituts.add(new LinkedList());
        }
        
        for(Map<String,String> sub : substitucions) {
            boolean trobat = false;
            //Iterator<List<OrdinaryLiteral>> it_pS = possiblesSubstituts.iterator();
            List<List<OrdinaryLiteral>> subs = new LinkedList<>();
            Iterator<List<Literal>> it = this.partDreta.iterator();
            int it_pS = 0;
            while (it.hasNext() && !trobat) {
                List<Literal> pD = it.next();
                
                List<OrdinaryLiteral> substitut = new LinkedList<>();
                boolean existencial = this.teExistencialsPart(pD);
                for(Literal l : pD) {
                   OrdinaryLiteral l_or = (OrdinaryLiteral) l;
                   OrdinaryLiteral nouFet = (OrdinaryLiteral) l.getLiteralAfterSubstitution(sub);
                   if(existencial) {
                        if(!UtilsChase.son_constants_o_null(nouFet.getTerms())) {
                            nouFet = UtilsChase.substituirVariablesPerNulls(nouFet);
                            if(!UtilsChase.existeixAmbNulls(nouFet,fetsTotals)) {
                                substitut.add(nouFet);
                            }  
                        }
                        else if(!UtilsChase.existeix(l_or, sub, fetsTotals)) {
                            substitut.add(nouFet);
                        }
                        
                    }
                    else {
                        if(!UtilsChase.existeix(l_or, sub, fetsTotals) && UtilsChase.son_constants_o_null(l_or.getLiteralAfterSubstitution(sub).getTerms())) {
                            substitut.add(nouFet);
                        }
                    }

                }
                if(substitut.isEmpty()) trobat = true;
                else {
                    subs.add(it_pS,substitut);
                }
                it_pS++;
            }
            if(!trobat) {
                for(int i = 0; i < this.partDreta.size(); i++) {
                    possiblesSubstituts.get(i).addAll(subs.get(i));
                }
            }
            
        }
        return possiblesSubstituts;
    }

    
}
