/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.logicschemaRGD;

import edu.upc.mpi.BaseDeDades.Connector;
import edu.upc.mpi.chase.Chase;
import edu.upc.mpi.chase.UtilsChase;
import edu.upc.mpi.logicschema.LogicSchema;
import edu.upc.mpi.logicschema.NormalClause;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschema.Predicate;
import edu.upc.mpi.logicschema.Term;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author guille
 */
public class LogicSchemaRGD extends LogicSchema{
    private LinkedList<RGD> RGDs; //Totes les RGD
    private LinkedList<EGD> EGDs; //Totes les EGD
    private LinkedList<OrdinaryLiteral> fetsEntrada; //Tots els fets d'inici de l'esquema
    
    public LogicSchemaRGD() {
        super();
        this.RGDs = new LinkedList<RGD>();
        this.EGDs = new LinkedList<EGD>();
        this.fetsEntrada = new LinkedList<OrdinaryLiteral>();
    }
    /**
         * @return una llista amb totes les RGD del sistema.
         */
    public List<RGD> getAllRGDs() {
	return new LinkedList(RGDs);
    }
    /**
         * @return una llista amb tots els EGD del sistema
         */
    public List<EGD> getAllEGDs() {
        return new LinkedList(EGDs);
    }
    /**
     * 
     * @return llista amb tots els fets d'entrada del sistema.
     */
    public List<OrdinaryLiteral> getAllFetsEntrada() {
        return new LinkedList(fetsEntrada);
    }
    
    /**
     * Afegir un fet a la llista de fets inicials, tot comprovant que els termes siguin constants o LabelledNull.
     * @param fet 
     */
    public void addFetEntrada(OrdinaryLiteral fet) {
        boolean constants = true;
        for (Term t : fet.getTerms()) {
            if (t.isVariable()) {
                 
                constants = false;
                break;
                
            }
        }
        if (constants) fetsEntrada.add(fet);
    }   
    /**
         * Afegeix la nova RGD al sistema i els seus predicats 
         * que no són ja al sistema.
         * @param regla
         */
    public void addRGD(RGD regla) {
        List<NormalClause> safeRules = new LinkedList<NormalClause>();
        safeRules.add(regla);
        LinkedList<Predicate> predicats = getAllPredicates();
        for(NormalClause safeRule: safeRules){
            for(Predicate p: safeRule.getAllPredicatesClosure()){
                if(!predicats.contains(p)) {
                    super.addPredicate(p);
                }
            }
            if(!RGDs.contains((RGD)safeRule))RGDs.add((RGD)safeRule);
        }
    }
    /**
         * Afegeix l'RGD al sistema sense comprovar si ja hi és o no.
         * @param regla
         */
    public void addRGDWithoutCheckingExistance (RGD regla) {
        List<NormalClause> safeRules = new LinkedList<NormalClause>();
        safeRules.add(regla);
        for(NormalClause safeRule: safeRules){
            RGDs.add((RGD)safeRule);
        }
    }
    /**
         * Afegeix la nova EGD al sistema i els seus predicats 
         * que no són ja al sistema.
         * @param regla
         */
    
    public void addEGD(EGD regla) {
        List<NormalClause> safeRules = new LinkedList<NormalClause>();
        safeRules.add(regla);
        LinkedList<Predicate> predicats = getAllPredicates();
        for(NormalClause safeRule: safeRules){
            for(Predicate p: safeRule.getAllPredicatesClosure()){
                if(!predicats.contains(p)) {
                    super.addPredicate(p);
                }
            }
            if(!EGDs.contains((EGD)safeRule))EGDs.add((EGD)safeRule);
        }
    }
    /**
         * Afegeix l'EGD al sistema sense comprovar si ja hi és o no.
         * @param regla
         */
    public void addEGDWithoutCheckingExistance (EGD regla) {
        List<NormalClause> safeRules = new LinkedList<NormalClause>();
        safeRules.add(regla);
        for(NormalClause safeRule: safeRules){
            EGDs.add((EGD)safeRule);
        }
    }
    
    public void afegirLiteralsBaseDeDades(String nomBD, String url, String nom_usuari, String clau) throws Exception {
        Connector n = new Connector(url,nom_usuari,clau);
        List<OrdinaryLiteral> dadesbd = n.obtenirTuplesBD(nomBD);
        this.fetsEntrada = (LinkedList<OrdinaryLiteral>) UtilsChase.afegirLiterals(fetsEntrada, dadesbd);
    }
    
    @Override
    public String toString() {
        String result = super.toString();
        result +="% RGDs\n";        
        for(RGD r: this.getAllRGDs()){
            result += (r.toString())+"\n";
        }
        result +="% EGDs\n"; 
        for(EGD r: this.getAllEGDs()){
            result += (r.toString())+"\n";
        }
        result += "\n";
        return result;
    }
    
    
    public boolean teDisjuncions() {
        for(RGD r : this.RGDs) {
            if(r.teDisjuncions()) return true;
        }
        return false;
    }
    
    /**
         * This procedure prints all the schema in a recognisible format for SVTe
         * in the file given by parameter
         *
         * @param file file in which the schema will be printed
         * @throws Exception
         */
    @Override
    public void printSchema(File file) throws Exception {
            printSchema(file.getCanonicalPath());
    }
        
        /**
         * This procedure prints all the schema in a recognisible format for SVTe
         * in the file whose name is filename
         *
         * @param fileName fileName of the file in which the schema will be printed
         * @throws Exception
         */
    @Override
    public void printSchema(String fileName) throws Exception {
            FileWriter writer = null;
            PrintWriter pw = null;
            try {
                    writer = new FileWriter(fileName);
                    pw = new PrintWriter(writer);
                    printSchema(pw);
            } finally {
                    if (pw != null) pw.close();
                    if (writer != null) writer.close();
            }
    }
    /**
     * This procedure prints all the schema in a recognisible format for SVTe in
     * the given printer
     *
     * @param pro uml project
     * @param pw printer
     * @throws Exception
     */
    @Override
    public void printSchema(PrintWriter pw) {
        pw.println(this.toString());
    }

    Term getTerm(String term1) {
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
