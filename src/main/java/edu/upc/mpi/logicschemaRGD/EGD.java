/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.logicschemaRGD;
import edu.upc.mpi.chase.Chase;
import edu.upc.mpi.chase.UtilsChase;
import edu.upc.mpi.logicschema.BuiltInLiteral;
import edu.upc.mpi.logicschema.Literal;
import edu.upc.mpi.logicschema.NormalClause;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschema.Term;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
/**
 *
 * @author Guillermo Ezcurra
 */
public class EGD extends NormalClause{
    private final BuiltInLiteral head;
    
    /**
     * Construeix una nova regla EGD utilitzant la llista de literals passada per paràmetre i la seva head;
     * 
     * @param head
     * @param literals
     */
    public EGD(BuiltInLiteral head, List<Literal> literals) {
        super(literals);
        this.head = head;
    }
    public BuiltInLiteral getHead(){
            return head;
    }
    @Override
    public String toString()
    {
        
        String result = new String();
        boolean commaRequired=false;
        for(Literal lit: super.getLiterals()){
            if(commaRequired)result+=", ";
            result+=lit.toString();
            commaRequired=true;
        }
        result += " -> ";
        result += head.toString();
        return result;
    }
    @Override
    protected String getHeadAsString() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected int hashCodeSpecific() {
        return this.head.hashCode();
    }

    @Override
    public NormalClause copyChangingBody(List<Literal> list) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Map<String, String> getVariableToVariableSubstitutionForHead(NormalClause nc) {
        if (nc instanceof EGD) {
            EGD e = (EGD) nc;
            Map<String, String> substitution1 = new HashMap<>();
            return this.head.getVariableToVariableSubstitution(substitution1,(Literal) e.getHead());
            
        }
        return null;
    }
    
    public List<OrdinaryLiteral> aplicarSubstitucions(List<Map<String,String>> substitucions, List<OrdinaryLiteral> fetsTotals) {
        List<OrdinaryLiteral> nousFetsRegla = new LinkedList<>();
        for(Map<String,String> sub : substitucions) {
            Term te = this.head.getLeftTerm();
            Term td = this.head.getRightTerm();
            //Comprovem si el builtinliteral és x = 'constant' o és x = y
            if (td.isVariable()) {                     
                if(sub.containsKey(te.getName()) && sub.containsKey(td.getName())) {
                    if(!sub.get(te.getName()).equals(sub.get(td.getName()))) {
                        if(sub.get(te.getName()).startsWith("#") && sub.get(td.getName()).startsWith("#")) {
                            sub.put(te.getName(), sub.get(td.getName()));
                        }
                        else if (sub.get(te.getName()).startsWith("#")) {
                            sub.put(te.getName(), sub.get(td.getName()));
                        }
                        else if (sub.get(td.getName()).startsWith("#")) {
                            sub.put(td.getName(), sub.get(te.getName()));
                        }
                        else return null; //La regla falla si les dues variables no tenen mateix valor.
                    }
                }
            }
            else {
                //Terme dreta és constant
                if(sub.containsKey(te.getName())) {
                    if(sub.get(te.getName()).startsWith("#")) {
                        //La variable del terme de l'esquerra és LabelledNull.
                        sub.put(te.getName(), td.getName());
                    }
                    else {
                        //La regla falla si no es compleix que la variable tingui el valor especificat.
                        if (!sub.get(te.getName()).equals(td.getName())) return null; 
                    }
                }
            } 
        }
        for (Literal l : this.getLiterals()) {
            for(Map<String,String> sub : substitucions) {
                OrdinaryLiteral l_or = (OrdinaryLiteral) l;
                if(!UtilsChase.existeix(l_or, sub, fetsTotals) && UtilsChase.son_constants_o_null(l_or.getLiteralAfterSubstitution(sub).getTerms())) {
                    OrdinaryLiteral nouFet = (OrdinaryLiteral) l.getLiteralAfterSubstitution(sub);
                    nousFetsRegla.add(nouFet);
                }
            }
            fetsTotals = UtilsChase.afegirLiterals(fetsTotals, nousFetsRegla);
        }
        
        return nousFetsRegla;
    }
    
}
