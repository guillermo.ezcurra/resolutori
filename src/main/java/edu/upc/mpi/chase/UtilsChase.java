/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.chase;

import edu.upc.mpi.logicschema.Atom;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschema.Term;
import edu.upc.mpi.logicschemaRGD.LabelledNull;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author guille
 */
public class UtilsChase {
    /**
     * Retorna cert si els dos literals tenen el mateix predicat, el mateix nombre de termes i si és possible fer la substitució de les seves variables.
     * @param l1
     * @param l2
     * @return 
     */
    
    protected static boolean parcialment_iguals(OrdinaryLiteral l1, OrdinaryLiteral l2) {
        Map<String, String> substitution  = l1.getUnification(l2);
        return l1.getPredicate().equals(l2.getPredicate()) && l1.getTerms().size() == l2.getTerms().size() && l1.isVariableUnifiable(l2, substitution);
    }
    /**
     * 
     * @param termes
     * @return Cert si tots els termes són constants, fals altrament.
     */
    public static boolean son_constants(List<Term> termes) {
        for (Term t : termes) {
            if (!t.isConstant()) return false;
        }
        return true;
    }
    /**
     * 
     * @param termes
     * @return Cert si tots els termes són constants o nulls, fals altrament.
     */
    public static boolean son_constants_o_null(List<Term> termes) {
        
        for (Term t : termes) {
            if (!t.isConstant())
                if(!(t.getName().startsWith("#"))) return false;
        }
        return true;
    }
    /**
     * 
     * @param termes
     * @return Cert si tots conté algun terme que sigui null.
     */
    public static boolean conteNulls(List<Term> termes) {
        
        for (Term t : termes) {
                if(t.getName().startsWith("#")) return true;
        }
        return false;
    }
    
    
    /**
     * S'evalua si les dues substitucions tenen una entrada amb la mateixa clau i diferent valor
     * @param substitucio1
     * @param substitucio2
     * @return Booleà que és cert si substitucio1 i substitucio2 tenen entrades amb mateixa clau i diferent valor, altrament és fals.
     */
    public static boolean conte_clau_diferent_valor(Map<String,String> substitucio1, Map<String,String> substitucio2) {
        for(Map.Entry<String,String> entry : substitucio1.entrySet()) {
            if(substitucio2.containsKey(entry.getKey())) {
                String valor = substitucio2.get(entry.getKey());
                if (!valor.equals(entry.getValue())) return true;
            }
        }
        return false;
    }
    
    public static List<OrdinaryLiteral> eliminarLiterals(List<OrdinaryLiteral> llista_principal, List<OrdinaryLiteral> elementsAEsborrar) {
        for (OrdinaryLiteral ol : elementsAEsborrar) {
            if(llista_principal.contains(ol)) {
                llista_principal.remove(ol);
            }
        }
        return llista_principal;
    }
    
    /**
     * Afegeix els literals de nousElements a la llista_principal només si no hi són.
     * @param llista_principal
     * @param nousElements
     * @return Llista amb la fusió de llista_principal i nousElements
     */
    public static List<OrdinaryLiteral> afegirLiterals(List<OrdinaryLiteral> llista_principal, List<OrdinaryLiteral> nousElements) {
        for (OrdinaryLiteral ol : nousElements) {
            if(!llista_principal.contains(ol)) {
                llista_principal.add(ol);
            }
        }
        return llista_principal;
    }
    /**
     * Avalua si, el literal fetDreta, després d'aplicar la substitució, coincideix amb cap dels literals de fetsEntrada
     * @param fetDreta
     * @param substitucio
     * @param fetsEntrada
     * @return 
     */
    public static boolean existeix(OrdinaryLiteral fetDreta, Map<String,String> substitucio, List<OrdinaryLiteral> fetsEntrada) {
        OrdinaryLiteral fetDreta_aux = (OrdinaryLiteral) fetDreta;
        fetDreta_aux = fetDreta_aux.getLiteralAfterSubstitution(substitucio);
        return fetsEntrada.contains(fetDreta_aux);
    }
    /**
     * Avalua si, el literal nouFet, coincideix amb cap dels literals de fetsEntrada.
     * @param nouFet té un o més termes que són nulls
     * @param fetsEntrada
     * @return 
     */
    public static boolean existeixAmbNulls(OrdinaryLiteral nouFet, List<OrdinaryLiteral> fetsEntrada) {
        for(OrdinaryLiteral ol : fetsEntrada) {
            if(ol.getPredicate().equals(nouFet.getPredicate()) && (ol.getTerms().size() == nouFet.getTerms().size())) {
                Iterator<Term> it_nouFet = nouFet.getTerms().iterator();
                Iterator<Term> it_ol = ol.getTerms().iterator();
                boolean existeix = true;
                while(it_nouFet.hasNext() && it_ol.hasNext() && existeix) {
                    Term t1 = it_nouFet.next();
                    Term t2 = it_ol.next();
                    if(t1.isVariable() | t2.isVariable()) existeix = false;
                    else if(t1.isConstant() && t2.isConstant()) {
                        if(!t1.equals(t2)) existeix = false;
                    }
                    else if(t1 instanceof LabelledNull) {
                        if(!t2.getName().startsWith("#")) existeix = false;
                    }
                }
                if(existeix) return true;
            }
        }
        return false;
    }
    
    /**
     * Canvia les variables del literal en què s'ha aplicat la substitució per Labelled Nulls
     * @param o Literal amb variables
     * @return 
     */
    public static OrdinaryLiteral substituirVariablesPerNulls(OrdinaryLiteral o) {
        List<Term> termes = new LinkedList();
        for(Term t : o.getTerms()) {
            if(!t.isVariable()) termes.add(t);
            else termes.add(new LabelledNull());
        }
        OrdinaryLiteral o1 = new OrdinaryLiteral(new Atom(o.getPredicate(),termes));
        return o1;
    }
    
    /**
     * Retorna cert si el literal ol1 és substituible per ol2
     * @param ol1 Literal que es vol comprovar
     * @param ol2 Literal amb què es compara
     * @return 
     */
    public static boolean literalSubstituible(OrdinaryLiteral ol1, OrdinaryLiteral ol2) {
        if(!(UtilsChase.conteNulls(ol2.getTerms()))) {
            if(ol1.getPredicate().equals(ol2.getPredicate()) && ol1.getTerms().size() == ol2.getTerms().size()) {
                Iterator<Term> it1 = ol1.getTerms().iterator();
                Iterator<Term> it2 = ol2.getTerms().iterator();
                while(it1.hasNext() && it2.hasNext()) {
                    Term t1 = it1.next();
                    Term t2 = it2.next();
                    if(t1 instanceof LabelledNull) {
                        if(t2 instanceof LabelledNull || t2.isVariable()) return false;
                    }
                    else {
                        if(!(t1.equals(t2))) return false;
                    }
                }
                return true;
            }
            else return false;
        }
        else return false;
    }
}
