/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.chase;

import edu.upc.mpi.logicschema.Atom;
import edu.upc.mpi.logicschema.BuiltInLiteral;
import edu.upc.mpi.logicschema.Literal;
import edu.upc.mpi.logicschema.NormalClause;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschema.Term;
import edu.upc.mpi.logicschemaRGD.EGD;
import edu.upc.mpi.logicschemaRGD.LabelledNull;
import edu.upc.mpi.logicschemaRGD.RGD;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 *
 * @author guille
 */
public class Chase {
    //private final List<OrdinaryLiteral> fetsEntrada; 
    
    public Chase() {
    }
    
    
    
    
    public List<Map<String,String>> chaseRecursiu(ListIterator<Literal> itEsquerra, List<OrdinaryLiteral> FetsEntrada, Map<String,String> substitucio, List<Map<String,String>> substitucions) {
        if (!itEsquerra.hasNext()) {
            substitucions.add(substitucio);
            return substitucions;
        }
        else {   
            OrdinaryLiteral fet = (OrdinaryLiteral)itEsquerra.next();
            for (OrdinaryLiteral l : FetsEntrada) {
                Map<String,String> nova_substitucio = fet.getUnification((OrdinaryLiteral)l);
                if(nova_substitucio != null) {
                    if(UtilsChase.conte_clau_diferent_valor(substitucio,nova_substitucio)) {
                        Map<String,String> sub_aux = new HashMap();
                        sub_aux.putAll(substitucio);
                        sub_aux.putAll(nova_substitucio);
                        substitucions = chaseRecursiu(itEsquerra, FetsEntrada, sub_aux, substitucions);
                    }
                    else {
                        substitucio.putAll(nova_substitucio);
                        substitucions = chaseRecursiu(itEsquerra,FetsEntrada, substitucio, substitucions);
                    }
                    
                }
            }
        itEsquerra.previous();
        return substitucions;
        }
    }
    
    public List<OrdinaryLiteral> chase(List<NormalClause> regles, List<OrdinaryLiteral> fetsEntrada) {
        List<OrdinaryLiteral> nousFetsIteracio = new LinkedList<>();
        List<OrdinaryLiteral> nousFets = new LinkedList<>();
        List<OrdinaryLiteral> fetsTotals = new LinkedList<>();
        fetsTotals.addAll(fetsEntrada);
        int it = 0;
        while (!nousFetsIteracio.isEmpty() | it == 0) {
            fetsTotals = UtilsChase.afegirLiterals(fetsTotals, nousFets);
            nousFetsIteracio = new LinkedList<>();
            for (NormalClause regla : regles) {             
                fetsTotals = UtilsChase.afegirLiterals(fetsTotals, nousFetsIteracio);
                List<OrdinaryLiteral> nousFetsRegla = new LinkedList<>();
                Map<String,String> substitucio  = inicialitzar_substitucio(regla.getLiterals().get(0),fetsTotals);
                if (substitucio != null) {      
                    List<Map<String,String>> substitucions1 = new LinkedList<>();
                    List<Map<String,String>> substitucions = chaseRecursiu(regla.getLiterals().listIterator(), fetsTotals, substitucio,substitucions1);
                    if (substitucions != null) {
                        //Aplicar substitucions als literals de la dreta de l'RGD
                        if (regla instanceof RGD) {
                            RGD reglaRGD = (RGD) regla;
                            fetsTotals = UtilsChase.afegirLiterals(fetsTotals, nousFetsIteracio);
                            nousFetsRegla = reglaRGD.aplicarSubstitucions(substitucions, fetsTotals, 0);
                        }
                        //Aplicar substitucions als literals de l'EGD
                        else if(regla instanceof EGD) {
                            EGD reglaEGD = (EGD) regla;
                            fetsTotals = UtilsChase.afegirLiterals(fetsTotals, nousFetsIteracio);
                            nousFetsRegla = reglaEGD.aplicarSubstitucions(substitucions, fetsTotals);
                            if(nousFetsRegla == null) return null;
                        }
                    }
                }
                nousFetsIteracio = UtilsChase.afegirLiterals(nousFetsIteracio, nousFetsRegla);  
            }
            nousFets = UtilsChase.afegirLiterals(nousFets, nousFetsIteracio);
            it++;
        }
        return nousFets;
    }
    
    
    public void chaseDisjuncionsRecursiu(NodeArbre node, List<NormalClause> regles, List<OrdinaryLiteral> fetsTotals, int it) {
        //NodeArbre node = new NodeArbre(new LinkedList<OrdinaryLiteral>());
        List<OrdinaryLiteral> nousFetsIteracio = new LinkedList<>();
        if(!(node.obteValor().isEmpty()) | it == 0) {
            it++;
            int numIteracio = 0;
            while (!nousFetsIteracio.isEmpty() | numIteracio == 0) {
                nousFetsIteracio = new LinkedList<>();
                for (NormalClause regla : regles) {
                    List<OrdinaryLiteral> nousFetsRegla = new LinkedList<>();
                    fetsTotals = UtilsChase.afegirLiterals(fetsTotals, node.obteValor());
                    Map<String,String> substitucio  = inicialitzar_substitucio(regla.getLiterals().get(0),fetsTotals);
                    if (substitucio != null) {
                        List<Map<String,String>> substitucions1 = new LinkedList<>();
                        List<Map<String,String>> substitucions = chaseRecursiu(regla.getLiterals().listIterator(), fetsTotals, substitucio,substitucions1);
                        if (substitucions != null) {
                            if (regla instanceof RGD) {
                                RGD reglaRGD = (RGD) regla;
                                if(reglaRGD.teDisjuncions()) {
                                    List<List<OrdinaryLiteral>> llista_nousFetsRegla = new LinkedList<>();
                                    llista_nousFetsRegla = reglaRGD.aplicarSubstitucionsDisjuncio(substitucions, fetsTotals);
                                    if(!llista_nousFetsRegla.isEmpty()) {
                                        for(List<OrdinaryLiteral> nFR : llista_nousFetsRegla) {
                                            if(!nFR.isEmpty()) {
                                                fetsTotals = UtilsChase.afegirLiterals(fetsTotals, nFR);
                                                NodeArbre node1 = node.afegirFill(nFR);
                                                chaseDisjuncionsRecursiu(node1,regles,fetsTotals,it);
                                                fetsTotals = UtilsChase.eliminarLiterals(fetsTotals, nFR);
                                            }
                                        }
                                    } 
                                }
                                else {
                                   //fetsTotals = UtilsChase.afegirLiterals(fetsTotals, nousFetsIteracio);
                                   nousFetsRegla = reglaRGD.aplicarSubstitucions(substitucions, fetsTotals,0);
                                   node.afegirValor(nousFetsRegla);
                                   nousFetsIteracio = UtilsChase.afegirLiterals(nousFetsIteracio, nousFetsRegla);
                                }

                            }
                            else if(regla instanceof EGD) {
                                EGD reglaEGD = (EGD) regla;
                                //fetsTotals = UtilsChase.afegirLiterals(fetsTotals, nousFetsIteracio);
                                nousFetsRegla = reglaEGD.aplicarSubstitucions(substitucions, fetsTotals);
                                //if(nousFetsRegla == null) return null;
                                node.afegirValor(nousFetsRegla);
                                nousFetsIteracio = UtilsChase.afegirLiterals(nousFetsIteracio, nousFetsRegla);
                            }
                        }
                    }  
                } 
            numIteracio++;
            }
        }
        
    }
    
    
    
    
    protected static Map<String,String> inicialitzar_substitucio(Literal fetRGD, List<OrdinaryLiteral> fetsEntrada) {
        Map<String,String> substitucio  = new HashMap();
        OrdinaryLiteral fet = (OrdinaryLiteral) fetRGD;
        //TODO: iniciar la substitucio, aquí o a la funció principal del chase.
        for(OrdinaryLiteral l : fetsEntrada) {
            Map<String,String> substitucio1 = fet.getUnification(l);
            if (substitucio1 != null) {
               return substitucio1;
            }
        }
        return null;
            
    }
    
    
    //TODO: fer text de prova d'esborrar nulls
    public List<OrdinaryLiteral> esborrarNulls(List<OrdinaryLiteral> fetsTotals) {
        List<OrdinaryLiteral> literals_esborrats = new LinkedList<>();
        Iterator<OrdinaryLiteral> it1 = fetsTotals.iterator();
        while(it1.hasNext()) {
            OrdinaryLiteral ol1 = it1.next();
            if(UtilsChase.conteNulls(ol1.getTerms())) {
                Iterator<OrdinaryLiteral> it2 = fetsTotals.iterator();
                while(it2.hasNext()) {
                    OrdinaryLiteral ol2 = it2.next();
                    if(UtilsChase.literalSubstituible(ol1,ol2)) {
                        literals_esborrats.add(ol1);
                        break;
                    }

                }
            }
        }
        return literals_esborrats;
    }
    
}

    
