/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.chase;

import edu.upc.mpi.logicschema.OrdinaryLiteral;
import java.util.ArrayList;
import java.util.List;

/**
 * Node de l'arbre que s'utilitza per representar les diferents reparacions en cas que hi hagi RGDs amb disjuncions a l'esquema.Implementació inspirada en https://stackoverflow.com/questions/19330731/tree-implementation-in-java-root-parents-and-children/40622616#40622616
 * 
 * @author guille
 * @param <List>
 * @param Llista d'ordinary literals
 */
public class NodeArbre{
    private List<OrdinaryLiteral> valor = null;
    private List<NodeArbre> fills = new ArrayList<>();
    
    public NodeArbre(List<OrdinaryLiteral> dades) {
        this.valor = dades;
    }
    public void afegirFill(NodeArbre fill) {
        this.fills.add(fill);
    }
    public NodeArbre afegirFill(List<OrdinaryLiteral> dades) {
        NodeArbre nouFill = new NodeArbre(dades);
        fills.add(nouFill);
        return nouFill;
    }
    public void afegirFills(List<NodeArbre> fills) {
        this.fills.addAll(fills);
    }
    public List<NodeArbre> obteFills() {
        return fills;
    }
    public List<OrdinaryLiteral>  obteValor() {
        return valor;
    }
    public boolean esFulla() {
        return fills.isEmpty();
    }
    public void setValor(List<OrdinaryLiteral>  dades) {
        this.valor = dades;
    }
    public void afegirValor(List<OrdinaryLiteral>  dades) {
        this.valor = UtilsChase.afegirLiterals(this.valor, dades);
    }
    
    public static String serialize( NodeArbre root ) {
        StringBuilder sb = new StringBuilder();
        if ( root != null ) {
            sb.append( root.obteValor().toString() ).append( "," );
            for ( NodeArbre child : root.obteFills() ) {
                sb.append( serialize( child ) );
            }
            sb.append( ")" );
        }
        return sb.toString();
    }
}
