
import edu.upc.mpi.BaseDeDades.Connector;
import edu.upc.mpi.antlr.LogicSchemaAmpliatParser;
import edu.upc.mpi.chase.Chase;
import edu.upc.mpi.chase.NodeArbre;
import edu.upc.mpi.chase.UtilsChase;
import edu.upc.mpi.logicschema.NormalClause;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschemaRGD.LogicSchemaRGD;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author guille
 */
public class TestGlobal2 {
        public static void main(String[] args) throws Exception{
            String fets_entrada = new String();
            String esquema_entrada = new String();
            Scanner scan = new Scanner(System.in);
            System.out.println("Nom de la base de dades:");
            String nomBD = scan.nextLine();
            System.out.println("URL de la base de dades:");
            String urlBD = scan.nextLine();
            System.out.println("Usuari de la base de dades:");
            String userBD = scan.nextLine();
            System.out.println("Clau de la base de dades:");
            String clauBD = scan.nextLine();
            Connector n = new Connector(urlBD,userBD,clauBD);    
            List<OrdinaryLiteral> fetsTaula = n.obtenirTuplesBD(nomBD);
            System.out.println("Fets carregats:");
            for (OrdinaryLiteral l : fetsTaula)  {
                System.out.println(l.toString());
            }
            System.out.println("Escriu les teves regles aquí, escriu prou quan hagis acabat");   
            while (scan.hasNextLine()) {
                String linia = scan.nextLine();
                if (!linia.equals("prou")) {
                    esquema_entrada += linia + "\n";
                }
                else break;
            } 
            LogicSchemaAmpliatParser instance1 = new LogicSchemaAmpliatParser(esquema_entrada);
            instance1.parse();
            LogicSchemaRGD esquema = instance1.getLogicSchemaRGD();
            for (OrdinaryLiteral l : fetsTaula)  {
                esquema.addFetEntrada(l);
            }
            List<NormalClause> regles = new LinkedList<>(esquema.getAllRGDs());
            regles.addAll(esquema.getAllEGDs());
            System.out.println("Regles de l'esquema:");
            for (NormalClause r : regles)  {
                System.out.println(r.toString());
            }
            List<OrdinaryLiteral> fetsReparacio =  new LinkedList<>();
            Chase ch = new Chase();
            NodeArbre inici = new NodeArbre(new LinkedList<>());
            if(esquema.teDisjuncions()) {
                ch.chaseDisjuncionsRecursiu(inici, regles, esquema.getAllFetsEntrada(), 0);
                System.out.println(NodeArbre.serialize(inici));
            }
            else {
                fetsReparacio = ch.chase(regles, esquema.getAllFetsEntrada());
            }
            if(fetsReparacio.isEmpty()) System.out.println("No hi ha solució per a una de les regles.");
            else {
                System.out.println("Fets de sortida:");
                fetsReparacio.forEach((OrdinaryLiteral fr) -> {
                    System.out.println(fr.toString());
                });
            }
            List<OrdinaryLiteral>  fetsTotals = new LinkedList<>(esquema.getAllFetsEntrada());
            fetsTotals = UtilsChase.afegirLiterals(fetsTotals, fetsReparacio);
            List<OrdinaryLiteral> fetsEsborrats = ch.esborrarNulls(fetsTotals);
            if(fetsEsborrats == null) System.out.println("No hi ha ha cap fet que cal esborrar de la base de dades.");
            else {
                System.out.println("Fets que cal esborrar de la base de dades:");
                fetsEsborrats.forEach((OrdinaryLiteral fe) -> {
                    System.out.println(fe.toString());
                });
            }
        }
}
