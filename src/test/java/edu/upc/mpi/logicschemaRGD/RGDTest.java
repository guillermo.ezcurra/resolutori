/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.logicschemaRGD;

import edu.upc.mpi.antlr.LogicSchemaAmpliatParser;
import edu.upc.mpi.logicschema.Literal;
import edu.upc.mpi.logicschema.NormalClause;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author guille
 */
public class RGDTest {
    
    public RGDTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }


    /**
     * Test of teExistencials method, of class RGD.
     */
    @Test
    public void testTeExistencials() {
        System.out.println("teExistencials");
        String esquema = "P(x) -> T(x,y)\n";
        LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(esquema);
        instance.parse();
        
        LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();
        
        List<RGD> resultatRGDs = resultLogicSchemaRGD.getAllRGDs();

        RGD instancergd = resultatRGDs.get(0);
        boolean expResult = true;
        boolean result = instancergd.teExistencials();
        assertEquals(expResult, result);
    }
    
    /**
     * Test per a comprovar que les RGD amb disjuncions creen múltiples llistes que es poden tractar.
     */
    @Test
    public void testPartsDretes() {
        System.out.println("testAfegirRGDDisjuncions");
        String rgd1 = "P(x) -> B(x) v P(x)\n";
        rgd1 += "P(x),H(y) -> F(x),Q(x) v P(x)\n";
        rgd1 += "P(x) -> J(x) v M(x) v F(x),Y(x)\n";
        LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(rgd1);
        instance.parse();
        
        LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();
        //esquema
        List<RGD> resultat = resultLogicSchemaRGD.getAllRGDs();
        if(!resultat.isEmpty()) {
            for(RGD r: resultat) {
                int it = 0;
                List<List<Literal>> llista_partsDretes = r.getPartsDretes();
                for (List<Literal> ll : llista_partsDretes) {
                    System.out.println("Part dreta num " + it);
                    for(Literal l: ll) System.out.println(l.toString());
                    it++;
                }
            }
        } 
            
    }
    
    /**
     * Test per a comprovar que les RGD amb disjuncions creen múltiples llistes que es poden tractar.
     */
    @Test
    public void testAplicarSubstitucionsDisjuncions() {
        System.out.println("testAplicarSubstitucionsDisjuncions");
        String rgd1 = "P(100)\n";
        rgd1 += "P(x) -> B(x) v P(x)\n";
        LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(rgd1);
        instance.parse();
        
        LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();
        //esquema
        List<RGD> resultat = resultLogicSchemaRGD.getAllRGDs();
        List<OrdinaryLiteral> fetsEntrada = resultLogicSchemaRGD.getAllFetsEntrada();
        if(!fetsEntrada.isEmpty()) {
            System.out.println("Fets d'entrada:");
            for (OrdinaryLiteral ol : fetsEntrada) {
                System.out.println(ol.toString());
            }
        }
        if(!resultat.isEmpty()) {
            for(RGD r: resultat) {
                int it = 0;
                List<List<Literal>> llista_partsDretes = r.getPartsDretes();
                for (List<Literal> ll : llista_partsDretes) {
                    System.out.println("Part dreta num " + it);
                    for(Literal l: ll) System.out.println(l.toString());
                    it++;
                }
            }
        } 
            
    }

    
}
