/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.logicschemaRGD;

import edu.upc.mpi.antlr.LogicSchemaAmpliatParser;
import edu.upc.mpi.logicschema.Literal;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschema.Term;
import java.io.File;
import java.io.PrintWriter;
import java.util.Base64;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author guille
 */
public class LogicSchemaRGDTest {
    
    public LogicSchemaRGDTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testAfegirLiteralsBaseDeDades() throws Exception {
        String nomBD = "Llibreria";
        String url = "jdbc:mysql://localhost:3306/Llibreria";
        String nom_usuari = "root";
        String clau = "Prova.123";
        String clauCodificada = Base64.getEncoder().encodeToString(clau.getBytes());
        LogicSchemaRGD instance = new LogicSchemaRGD();
        instance.afegirLiteralsBaseDeDades(nomBD, url, nom_usuari, clauCodificada);
        List<OrdinaryLiteral> fetsEntrada = instance.getAllFetsEntrada();
        // TODO review the generated test code and remove the default call to fail.
        for (OrdinaryLiteral ol : fetsEntrada) System.out.println(ol);
    }
    
    @Test
    public void testTeDisjuncions() {
        System.out.println("testTeDisjuncions");
        String rgd1 = "P(x) -> B(x) v Q(x)\n";
        LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(rgd1);
        instance.parse();
        
        LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();
        //esquema
        assertTrue(resultLogicSchemaRGD.teDisjuncions());
    }

   
   

   

  
    
}
