 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.logicschemaRGD;

import edu.upc.mpi.logicschema.Atom;
import edu.upc.mpi.logicschema.BuiltInLiteral;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschema.Predicate;
import edu.upc.mpi.logicschema.PredicateImpl;
import edu.upc.mpi.logicschema.Term;
import java.util.LinkedList;

/**
 *
 * @author guille
 */
public class LogicSchemaRGDTestHelper {
    public static Atom getAtom(LogicSchemaRGD ls, String predicateName, String[] terms){
        Predicate p = ls.getPredicate(predicateName);
        if(p == null){
            p = new PredicateImpl(predicateName, terms.length);
            ls.addPredicate(p);
        }
        
        LinkedList<Term> termsList = new LinkedList();
        for(String term: terms){
            if(term.equals("null")) termsList.add(new LabelledNull());
            else termsList.add(new Term(term));
        }
        
        return new Atom(p, termsList);
    }
    public static BuiltInLiteral getBuiltInLiteral(LogicSchemaRGD ls, String term1, String term2) {
       return LogicSchemaRGDTestHelper.getBuiltInLiteral(ls, term1, term2, "="); 
    }
    public static BuiltInLiteral getBuiltInLiteral(LogicSchemaRGD ls, String term1, String term2, String operador) {
       Term t1 = new Term(term1);
       Term t2 = new Term(term2);
       return new BuiltInLiteral(t1, t2, operador);
    }
    
    public static OrdinaryLiteral getOrdinaryLiteral(LogicSchemaRGD ls, String predicateName, String[] terms){
        return LogicSchemaRGDTestHelper.getOrdinaryLiteral(ls, predicateName, terms, true);
    }
    
    public static OrdinaryLiteral getOrdinaryLiteral(LogicSchemaRGD ls, String predicateName, String[] terms, boolean truth){
        Atom atom = LogicSchemaRGDTestHelper.getAtom(ls, predicateName, terms);
        return new OrdinaryLiteral(atom, truth);
    }
}

