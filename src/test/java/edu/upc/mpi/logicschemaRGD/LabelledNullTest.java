/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.logicschemaRGD;

import edu.upc.mpi.antlr.LogicSchemaAmpliatParser;
import edu.upc.mpi.antlr.LogicSchemaAmpliatParserTest;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschema.Term;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author guille
 */
public class LabelledNullTest {
    
    public LabelledNullTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }


    /**
     * Test of nameIsModifiable method, of class LabelledNull.
     */
    @Test
    public void testNameIsModifiable() {
        System.out.println("nameIsModifiable");
        String esquema = "P(100,null)\n";
        LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(esquema);
        instance.parse();
        
        LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();
        OrdinaryLiteral ol_variable = LogicSchemaAmpliatParserTest.getOrdinaryLiteral(resultLogicSchemaRGD, "J", new String[]{"x"});
        List<OrdinaryLiteral> ol_list = resultLogicSchemaRGD.getAllFetsEntrada();
        OrdinaryLiteral ol = ol_list.get(0);
        System.out.println("Abans del canvi: " + ol.toString());
        for(Term t : ol.getTerms()) {
            if(t.getName().startsWith("#")) {
                t.setName("2");
            }
        }
        System.out.println("Després del canvi: " + ol.toString());
        System.out.println("Abans del canvi: " + ol_variable.toString());
        for(Term t : ol_variable.getTerms()) {
            
                t.setName("2");
            
        }
        System.out.println("Després del canvi: " + ol_variable.toString());
    }

    
    
}
