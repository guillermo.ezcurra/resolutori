/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.antlr;

import edu.upc.mpi.logicschema.BuiltInLiteral;
import edu.upc.mpi.logicschema.Literal;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschemaRGD.EGD;
import edu.upc.mpi.logicschemaRGD.LogicSchemaRGD;
import edu.upc.mpi.logicschemaRGD.RGD;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import edu.upc.mpi.logicschemaRGD.LogicSchemaRGDTestHelper;
/**
 *
 * @author guille
 */
public class LogicSchemaAmpliatParserTest extends LogicSchemaRGDTestHelper {
    
    public LogicSchemaAmpliatParserTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
//    @BeforeAll
//    public static void setUpClass() {
//    }
//    
//    @AfterAll
//    public static void tearDownClass() {
//    }
//    
//    @BeforeEach
//    public void setUp() {
//    }
//    
//    @AfterEach
//    public void tearDown() {
//    }

    
//    @org.junit.jupiter.api.Test
//    public void testGetReadFile() {
//        System.out.println("getReadFile");
//        File file = null;
//        LogicSchemaAmpliatParser instance = null;
//        String expResult = "";
//        String result = instance.getReadFile(file);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
    /**
     * Test d'afegir diferents RGD i EGDs a un mateix logicSchemaRGD
     * @throws java.lang.Exception 
     */
    public void testLlegirMoltsRGDiEGD()throws Exception {
        System.out.println("testLlegirMoltsRGDiEGD");
        String esquema = "P(x) -> B(x)\n";
        esquema += "P(x),B(y) -> x = y\n";
        esquema += "B(y) -> F(y)\n";
        esquema += "T(x,y),T(x,z) -> y = z\n";
        LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(esquema);
        instance.parse();
        
        LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();
        
        List<RGD> resultatRGDs = resultLogicSchemaRGD.getAllRGDs();
        List<EGD> resultatEGDs = resultLogicSchemaRGD.getAllEGDs();
        System.out.println(esquema);
        resultatRGDs.forEach(r -> {
            System.out.println(r.toString());
        });
        resultatEGDs.forEach(e -> {
            System.out.println(e.toString());
        });
    }
    /**
     * Test of afegirRGD a un esquema lògic.
     * @throws java.lang.Exception
     */
    @org.junit.jupiter.api.Test
    public void testAfegirRGD() throws Exception{
        System.out.println("testAfegirRGD");
        String rgd1 = "P(x) -> B(x)\n";
        LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(rgd1);
        
        instance.parse();
        
        LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();
        //esquema
        List<RGD> resultat = resultLogicSchemaRGD.getAllRGDs();
        
        LogicSchemaRGD expectedLogicSchemaRGD = new LogicSchemaRGD();
        List<Literal> part_esquerra = new LinkedList();
        List<Literal> part_dreta = new LinkedList();
        List<List<Literal>> partsdretes = new LinkedList();
        part_esquerra.add(LogicSchemaAmpliatParserTest.getOrdinaryLiteral(expectedLogicSchemaRGD, "P", new String[]{"x"}));
        part_dreta.add(LogicSchemaAmpliatParserTest.getOrdinaryLiteral(expectedLogicSchemaRGD, "B", new String[]{"x"}));
        partsdretes.add(part_dreta);
        RGD rgd = new RGD(part_esquerra,partsdretes);
        RGD rgd_aux = resultat.get(0);
        if(!(rgd_aux.equals(rgd))) {
            fail("Esperàvem: "+rgd+"\n"+ " but result is: "+rgd_aux);
        }
        assertEquals(rgd.toString(), resultat.get(0).toString());
    }
    /**
     * Test d'afegir RGD amb disjuncions
     * @throws Exception 
     */
    @org.junit.jupiter.api.Test
    public void testAfegirRGDDisjuncions() throws Exception{
        System.out.println("testAfegirRGDDisjuncions");
        String rgd1 = "P(x) -> B(x) v P(x)\n";
        rgd1 += "P(x),H(y) -> F(x),Q(x) v P(x)\n";
        rgd1 += "P(x) -> J(x) v M(x) v F(x),Y(x)\n";
        LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(rgd1);
        instance.parse();
        
        LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();
        //esquema
        List<RGD> resultat = resultLogicSchemaRGD.getAllRGDs();
        if(!resultat.isEmpty()) for(RGD r: resultat) System.out.println(r.toString());

    }
    
    
    /**
     * Test of afegirEGD a un esquema lògic.
     * @throws java.lang.Exception
     */
    @org.junit.jupiter.api.Test
    public void testAfegirEGD() throws Exception{
        System.out.println("testAfegirEGD");
        String egd1 = "P(x),B(y) -> x = y\n";
        LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(egd1);
        
        instance.parse();
        
        LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();
        //esquema
        List<EGD> resultat = resultLogicSchemaRGD.getAllEGDs();
        
        LogicSchemaRGD expectedLogicSchemaRGD = new LogicSchemaRGD();
        List<Literal> body = new LinkedList();
        BuiltInLiteral head = this.getBuiltInLiteral(expectedLogicSchemaRGD, "x", "y");
        body.add(this.getOrdinaryLiteral(expectedLogicSchemaRGD, "P", new String[]{"x"}));
        body.add(this.getOrdinaryLiteral(expectedLogicSchemaRGD, "B", new String[]{"y"}));
        EGD egd = new EGD(head,body);
        EGD egd_aux = resultat.get(0);
        System.out.println(egd.toString());
        if(!(egd_aux.equals(egd))) {
            fail("Esperàvem: "+egd.toString()+"\n"+ " but result is: "+egd_aux.toString());
        }
        assertEquals(egd.toString(), resultat.get(0).toString());
    }
    
//    @org.junit.jupiter.api.Test
//    public void testAfegirLiteralAmbNull() throws Exception{
//        System.out.println("testAfegirLiteralAmbNull");
//        String literal1 = "T(100,null) \n";
//        LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(literal1);
//        
//        instance.parse();
//        
//        LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();
//        //esquema
//        List<OrdinaryLiteral> resultat = resultLogicSchemaRGD.getAllFetsEntrada();
//        
//        LogicSchemaRGD expectedLogicSchemaRGD = new LogicSchemaRGD();
//        List<OrdinaryLiteral> resultat_exp = new LinkedList();
//        resultat_exp.add(LogicSchemaAmpliatParserTest.getOrdinaryLiteral(expectedLogicSchemaRGD, "T", new String[]{"100", "null"}));
//        OrdinaryLiteral ord_exp = resultat_exp.get(0);
//        OrdinaryLiteral ord = resultat.get(0);
//        if(!(ord.toString().equals("T(100,#0)"))) {
//            fail("Esperàvem: "+ord_exp.toString()+"\n"+ " but result is: "+ord.toString());
//        }
//        assertEquals("T(100,#0)", ord.toString());
//    }
    

    /**
     * Test of parse method, of class LogicSchemaAmpliatParser.
     */
//    @org.junit.jupiter.api.Test
//    public void testParse() {
//        System.out.println("parse");
//        LogicSchemaAmpliatParser instance = null;
//        instance.parse();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
