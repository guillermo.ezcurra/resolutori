/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.chase;

import edu.upc.mpi.antlr.LogicSchemaAmpliatParser;
import edu.upc.mpi.logicschema.Literal;
import edu.upc.mpi.logicschema.NormalClause;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschemaRGD.LogicSchemaRGD;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author guille
 */
public class ChaseTest {
    
    public ChaseTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }


    /**
     * Test of esborrarNulls method, of class Chase.
     */
    @Test
    public void testEsborrarNulls() {
        System.out.println("Test de la funció d'esborrarNulls:");
        String esquema = "A(100,null)\n";
        esquema+="A(100,2)\n";
        esquema+="A(100,1)\n";
        esquema+="A(200,null)\n";
        esquema+="P('guille',null)\n";
        esquema+="P('Pere',200)\n";
        esquema+="J('Joan')\n";
        esquema+="J(null)\n";
        esquema+="P('guille',20)\n";
        esquema+="B(100,null,null)\n";
        esquema+="B(100,2,4)\n";
        
        
        LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(esquema);
        instance.parse();
        
        LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();
        List<OrdinaryLiteral> ol_list = resultLogicSchemaRGD.getAllFetsEntrada();
        Chase ch = new Chase();
        List<OrdinaryLiteral> result = ch.esborrarNulls(ol_list);
        System.out.println("Literals a esborrar: ");
        for(OrdinaryLiteral ol: result) {
            System.out.println(ol.toString());
        }
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
