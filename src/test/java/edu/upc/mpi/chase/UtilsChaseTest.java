/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.upc.mpi.chase;

import edu.upc.mpi.antlr.LogicSchemaAmpliatParser;
import edu.upc.mpi.antlr.LogicSchemaAmpliatParserTest;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschema.Term;
import edu.upc.mpi.logicschemaRGD.LogicSchemaRGD;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author guille
 */
public class UtilsChaseTest {
    
    public UtilsChaseTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }


    /**
     * Test of literalSubstituible method, of class UtilsChase.
     */
    @Test
    public void testLiteralSubstituible() {
        System.out.println("LiteralSubstituible");
        String esquema = "P(100,null)\n";
        esquema+="P(100,2)\n";
        
        LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(esquema);
        instance.parse();
        
        LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();
        List<OrdinaryLiteral> ol_list = resultLogicSchemaRGD.getAllFetsEntrada();
        OrdinaryLiteral ol = ol_list.get(0);
        OrdinaryLiteral ol2 = ol_list.get(1);
        assertTrue(UtilsChase.literalSubstituible(ol, ol2));
    }
    
}
