/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import edu.upc.mpi.antlr.LogicSchemaAmpliatParser;
import edu.upc.mpi.chase.Chase;
import edu.upc.mpi.chase.NodeArbre;
import edu.upc.mpi.chase.UtilsChase;
import edu.upc.mpi.logicschema.NormalClause;
import edu.upc.mpi.logicschema.OrdinaryLiteral;
import edu.upc.mpi.logicschema.Term;
import edu.upc.mpi.logicschemaRGD.EGD;
import edu.upc.mpi.logicschemaRGD.LogicSchemaRGD;
import edu.upc.mpi.logicschemaRGD.LogicSchemaRGDTestHelper;
import edu.upc.mpi.logicschemaRGD.RGD;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner; 
import java.util.function.Consumer;
/**
 *
 * @author Guillermo Ezcurra
 */
public class TestGlobal {
        LogicSchemaRGD esquema;
        public static void main(String[] args){
            String fets_entrada = new String();
            String esquema_entrada = new String();
            Scanner scan = new Scanner(System.in);
            System.out.println("Escriu els fets inicials aquí, escriu prou quan hagis acabat");   
            while (scan.hasNextLine()) {
                String linia = scan.nextLine();
                if (!linia.equals("prou")) {
                    fets_entrada += linia + "\n";
                }
                else break;
            } 
            System.out.println("Escriu el teu esquema aquí, escriu prou quan hagis acabat");   
            while (scan.hasNextLine()) {
                String linia = scan.nextLine();
                if (!linia.equals("prou")) {
                    esquema_entrada += linia + "\n";
                }
                else break;
            } 
            LogicSchemaAmpliatParser instance1 = new LogicSchemaAmpliatParser(fets_entrada);
            instance1.parse();
            LogicSchemaRGD iniciLogicSchemaRGD = instance1.getLogicSchemaRGD();
            List<OrdinaryLiteral> fetsEntrada = new LinkedList<>();
            fetsEntrada.addAll(iniciLogicSchemaRGD.getAllFetsEntrada());
            //System.out.println("Això són les regles que has escrit: ");
            LogicSchemaAmpliatParser instance = new LogicSchemaAmpliatParser(esquema_entrada);
            instance.parse();
            LogicSchemaRGD resultLogicSchemaRGD = instance.getLogicSchemaRGD();
            
            
            //fetsEntrada.add(LogicSchemaRGDTestHelper.getOrdinaryLiteral(iniciLogicSchemaRGD, "A", new String[]{"100","1"}));
            System.out.println("Fets d'entrada");
            fetsEntrada.forEach((OrdinaryLiteral f) -> {
                System.out.println(f.toString());
            });
            List<OrdinaryLiteral> fetsTotals =  new LinkedList<>();
            List<OrdinaryLiteral> fetsReparacio =  new LinkedList<>();
            Chase ch = new Chase();
            List<NormalClause> regles = new LinkedList<>(resultLogicSchemaRGD.getAllRGDs());
            regles.addAll(resultLogicSchemaRGD.getAllEGDs());
            for(NormalClause r : regles) {
                System.out.println(r.toString());
            }
            NodeArbre inici = new NodeArbre(new LinkedList<>());
            if(resultLogicSchemaRGD.teDisjuncions()) {
                ch.chaseDisjuncionsRecursiu(inici, regles, fetsEntrada, 0);
                System.out.println(NodeArbre.serialize(inici));
            }
            else {
                fetsReparacio = ch.chase(regles, fetsEntrada);
            }
            if(fetsReparacio.isEmpty()) System.out.println("No hi ha solució per a una de les regles.");
            else {
                System.out.println("Fets de sortida:");
                fetsReparacio.forEach((OrdinaryLiteral fr) -> {
                    System.out.println(fr.toString());
                });
            }
            fetsTotals = UtilsChase.afegirLiterals(fetsTotals, fetsEntrada);
            fetsTotals = UtilsChase.afegirLiterals(fetsTotals, fetsReparacio);
            List<OrdinaryLiteral> fetsEsborrats = ch.esborrarNulls(fetsTotals);
            if(fetsEsborrats == null) System.out.println("No hi ha ha cap fet que cal esborrar de la base de dades.");
            else {
                System.out.println("Fets que cal esborrar de la base de dades:");
                fetsEsborrats.forEach((OrdinaryLiteral fe) -> {
                    System.out.println(fe.toString());
                });
            }
         }
        
}
